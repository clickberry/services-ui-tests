package stepDefinition;

import cucumber.api.java.en.*;

public class UserFeedback_Test {

	@When("^User opens Feedback page$")
	public void user_opens_Feedback_page() throws Throwable {
	   pages.IndexPage.OpenContacts();
	}

	@When("^Feedback page is opened$")
	public void feedback_page_is_opened() throws Throwable {
	    pages.FeedbackPage.CheckFeedback();
	}

	@Then("^User enters correct email$")
	public void user_enters_correct_email() throws Throwable {
	   pages.FeedbackPage.EnterEmail("feedback@test.ru");
	}

	@Then("^User enters correct Name$")
	public void user_enters_correct_Name() throws Throwable {
	  pages.FeedbackPage.EnterName("User Name"); 
	}

	@Then("^User enters correct Comment$")
	public void user_enters_correct_Comment() throws Throwable {
	  pages.FeedbackPage.EnterComment("Feedback Comment");  
	}

	@Then("^User sends Feedback$")
	public void user_sends_Feedback() throws Throwable {
	  pages.FeedbackPage.SendFeedback();
	  pages.FeedbackPage.FeedbackSucceed();
	}

	@Then("^Clickberry Support receives correct Feedback Email$")
	public void clickberry_Support_receives_Feedback_Email() throws Throwable {
	    pages.GooglePage.OpenEmail("clickqasup@gmail.com", "qa123456789");
	    pages.GooglePage.CheckFeedbackLetter("feedback@test.ru", "User Name", "Feedback Comment");
	    pages.GooglePage.GoogleLogout();
	}

	@Then("^User enters long email$")
	public void user_enters_long_email() throws Throwable {
		pages.FeedbackPage.EnterEmail("newverylongemailfortestingnewverylongemailnewtesting@testingnewverylongemail.com");
	}

	@Then("^User enters long Name$")
	public void user_enters_long_Name() throws Throwable {
		pages.FeedbackPage.EnterName("New User Name For Testing Long User Name 123456789"); 
	}

	@Then("^User enters long Comment$")
	public void user_enters_long_Comment() throws Throwable {
		pages.FeedbackPage.EnterComment("comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing 1234 new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new 12");   

	}

	@Then("^User enters symbols email$")
	public void user_enters_symbols_email() throws Throwable {
		pages.FeedbackPage.EnterEmail("feed-back_test.123@email.com");
	}

	@Then("^User enters symbols Name$")
	public void user_enters_symbols_Name() throws Throwable {
		pages.FeedbackPage.EnterName("~!@№$%^&()_+ 123456789"); 
	}

	@Then("^User enters symbols Comment$")
	public void user_enters_symbols_Comment() throws Throwable {
		pages.FeedbackPage.EnterComment("~!@№$%^&()_+ 123456789"); 
	}

	@Then("^User enters alphabet email$")
	public void user_enters_alphabet_email() throws Throwable {
		pages.FeedbackPage.EnterEmail("feedback123@test.ru");
	}

	@Then("^User enters alphabet Name$")
	public void user_enters_alphabet_Name() throws Throwable {
		pages.FeedbackPage.EnterName(" مشروع جديدनयाँ परियोजना新项目úéáćžä пользователь"); 
	}

	@Then("^User enters alphabet Comment$")
	public void user_enters_alphabet_Comment() throws Throwable {
		pages.FeedbackPage.EnterComment(" مشروع جديدनयाँ परियोजना新项目úéáćžä комментарий"); 
	}	
	
	@Then("^Clickberry Support receives long Feedback Email$")
	public void clickberry_Support_receives_long_Feedback_Email() throws Throwable {
		pages.GooglePage.OpenEmail("clickqasup@gmail.com", "qa123456789");
	    pages.GooglePage.CheckFeedbackLetter("newverylongemailfortestingnewverylongemailnewtesting@testingnewverylongemail.com", "New User Name For Testing Long User Name 123456789", "comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing 1234 new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new 12");
	    pages.GooglePage.GoogleLogout();
	}

	@Then("^Clickberry Support receives symbols Feedback Email$")
	public void clickberry_Support_receives_symbols_Feedback_Email() throws Throwable {
		pages.GooglePage.OpenEmail("clickqasup@gmail.com", "qa123456789");
	    pages.GooglePage.CheckFeedbackLetter("feed-back_test.123@email.com", "~!@№$%^&()_+ 123456789", "~!@№$%^&()_+ 123456789");
	    pages.GooglePage.GoogleLogout();
	}

	@Then("^Clickberry Support receives alphabet Feedback Email$")
	public void clickberry_Support_receives_alphabet_Feedback_Email() throws Throwable {
		pages.GooglePage.OpenEmail("clickqasup@gmail.com", "qa123456789");
	    pages.GooglePage.CheckFeedbackLetter("feedback123@test.ru", " مشروع جديدनयाँ परियोजना新项目úéáćžä пользователь", " مشروع جديدनयाँ परियोजना新项目úéáćžä комментарий");
	    pages.GooglePage.GoogleLogout();
	}
	
	@Then("^User enters incorrect email$")
	public void user_enters_incorrect_email() throws Throwable {
	    pages.FeedbackPage.EnterEmail("feedback.test.ru");
	}
	
	@Then("Feedback Submit button is disabled$")
	public void feedback_submit_button_is_disabled() throws Throwable {
	    pages.FeedbackPage.FeedbackDisabled();
	}

	@Then("^User cancels sending feedback$")
	public void user_cancels_sending_feedback() throws Throwable {
	    pages.FeedbackPage.FeedbackCancel();
	}

	@Then("^User enters absent email$")
	public void user_enters_absent_email() throws Throwable {
		pages.FeedbackPage.EnterEmail("");
	}

	@Then("^User enters incorrect Comment$")
	public void user_enters_incorrect_Comment() throws Throwable {
	    pages.FeedbackPage.EnterComment("comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing 1234 new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new very long comment new test comment for testing new 123");
	}

	@Then("^User enters absent Comment$")
	public void user_enters_absent_Comment() throws Throwable {
		pages.FeedbackPage.EnterComment("");
		
	}	
	
}
