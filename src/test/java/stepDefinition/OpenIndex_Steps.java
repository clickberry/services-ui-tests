package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import org.junit.Rule;

import com.codeborne.selenide.junit.ScreenShooter;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import selenium.util.PropertyLoader;
import cucumber.api.java.en.*;

public class OpenIndex_Steps {
	
	@Rule		
	public ScreenShooter screenShooter = ScreenShooter.failedTests();  
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }  
	
	@After
	
	public static void AfterTest() {
		
		pages.IndexPage.OpenIndex();		
	}
	
	@Given("^User is on Index Page$")
	public void user_is_on_Index_Page() {
		pages.IndexPage.OpenIndex();		
	}
	
	@Then("^Index Page is correctly displayed$")
	public void index_Page_is_correctly_displayed() throws Throwable {
	    pages.IndexPage.CheckIndex();
	}
	

}
