package stepDefinition;

import static com.codeborne.selenide.Selenide.close;
import cucumber.api.java.After;
import cucumber.api.java.en.*;

public class ProjectsSharing_Steps {
	
	@Given("^User clicks Share button for video$")
	public void user_clicks_Share_button_for_video() throws Throwable {
	    pages.IndexPage.ClickShareButton();
	}

	@Given("^User selects sharing to Facebook$")
	public void user_selects_sharing_to_Facebook() throws Throwable {
	    pages.IndexPage.FBShare();
	}

	@Then("^FB Login window is opened$")
	public void fb_Login_window_is_opened() throws Throwable {
	    pages.FacebookPage.FacebookPostLogin();
	}

	@Then("^User enters FB credentials$")
	public void user_enters_FB_credentials() throws Throwable {
	    pages.FacebookPage.FacebookLogin("qamary@mail.ru", "qa123456");
	}

	@Then("^FB Post window is opened$")
	public void fb_Post_window_is_opened() throws Throwable {
	   pages.FacebookPage.FBPostOpen();
	}

	@Then("^User created post to FB$")
	public void user_created_post_to_FB() throws Throwable {
		pages.FacebookPage.FBCreatePost();
	}

	@Then("^User checks created post on FB$")
	public void user_checks_created_post_on_FB() throws Throwable {
	    pages.FacebookPage.FBCreatedPostCheck();
	}

	@Then("^User goes back to Index page$")
	public void user_goes_back_to_Index_page() throws Throwable {
	    pages.IndexPage.OpenIndex();
	    close();
	}
	
	@Given("^User selects sharing to VK$")
	public void user_selects_sharing_to_VK() throws Throwable {
	    pages.IndexPage.VKShare();
	}

	@Then("^VK Login window is opened$")
	public void vk_Login_window_is_opened() throws Throwable {
	    pages.VkontaktePage.VKPostLoginOpen();
	}

	@Then("^User enters VK credentials$")
	public void user_enters_VK_credentials() throws Throwable {
	    pages.VkontaktePage.VkontakteLogin("79175986240", "qa123456");
	}

	@Then("^VK Post window is opened$")
	public void vk_Post_window_is_opened() throws Throwable {
	    pages.VkontaktePage.VKPostOpen();
	}

	@Then("^User created post to VK$")
	public void user_created_post_to_VK() throws Throwable {
	    pages.VkontaktePage.VKPostCreate();
	}

	@Then("^User checks created post on VK$")
	public void user_checks_created_post_on_VK() throws Throwable {
	    pages.VkontaktePage.VKCreatedPostCheck();
	}
	
	@Given("^User selects sharing to Twitter$")
	public void user_selects_sharing_to_Twitter() throws Throwable {
	    pages.IndexPage.TwitterShare();
	}
	
	@Then("^Twitter Post window is opened$")
	public void twitter_Post_window_is_opened() throws Throwable {
	   pages.TwitterPage.TwitterPostOpen();
	}
	
	@Then("^User enters Twitter credentials$")
	public void user_enters_Twitter_credentials() throws Throwable {
	    pages.TwitterPage.TwitterPostLogin("qamary@mail.ru", "qa123456");
	}

	@Then("^User creates post to Twitter$")
	public void user_created_post_to_Twitter() throws Throwable {
	    pages.TwitterPage.TwitterPostCreate();
	}

	@Then("^User checks created post on Twitter$")
	public void user_checks_created_post_on_Twitter() throws Throwable {
		//pages.TwitterPage.TwitterLastPostOpen();
	    pages.TwitterPage.TwitterCreatedPostCheck();
	}
	
	
	@Given("^User selects sharing to Google$")
	public void user_selects_sharing_to_Google() throws Throwable {
	    pages.IndexPage.GoogleShare();
	}

	@Then("^Google Login window is opened$")
	public void google_Login_window_is_opened() throws Throwable {
	    pages.GooglePage.OpenGoogleSignInPost();
	}

	@Then("^User enters Google credentials$")
	public void user_enters_Google_credentials() throws Throwable {
		pages.GooglePage.GoogleUserLogin("click4qa@gmail.com", "qa123456789");
	}

	@Then("^Google Post window is opened$")
	public void google_Post_window_is_opened() throws Throwable {
	    pages.GooglePage.OpenGooglePost();
	}

	@Then("^User created post to Google$")
	public void user_created_post_to_Google() throws Throwable {
	    pages.GooglePage.CreateGooglePost();
	}

	@Then("^User checks created post on Google$")
	public void user_checks_created_post_on_Google() throws Throwable {
	    pages.GooglePage.GoogleCreatedPostCheck();
	}

}
