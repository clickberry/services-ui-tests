package stepDefinition;

import cucumber.api.java.en.*;

public class SignUp_Steps {	

	@When("^New User navigates to Registration page$")
	public void new_User_navigates_to_Registration_page() throws Throwable {
		pages.IndexPage.OpenRegistrationPage();
		pages.SignUpPage.CheckRegistrationPage();
	}

	@When("^New FB User clicks Facebook icon$")
	public void new_FB_User_clicks_Facebook_icon() throws Throwable {
	    pages.SignUpPage.FbButtonClick();
	}

	@When("^Facebook Login window opens for new user$")
	public void facebook_Login_window_opens_for_new_user() throws Throwable {
	    pages.FacebookPage.OpenFacebookLogin();
	}

	@When("^New FB User enters Email and Password$")
	public void new_FB_User_enters_Email_and_Password() throws Throwable {
	    pages.FacebookPage.FacebookLogin("qamary@mail.ru", "qa123456");
	}

	@Then("^New User is redirected to Index Page$")
	public void new_User_is_redirected_to_Index_Page() throws Throwable {
		pages.IndexPage.checkIndexPagesIsLoaded();
	}

	@Then("^New User is logged in$")
	public void new_User_Name_is_displayed() throws Throwable {
		//pages.IndexPage.OpenUserMenu();
	    pages.IndexPage.CheckUserLoggedIn();
	}

	@Then("^New User Videos tab is displayed$")
	public void new_User_Videos_tab_is_displayed() throws Throwable {
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenVideosPage();
		pages.VideosPage.CheckMyVideosPage();
		//pages.VideosPage.CheckVideosAbsent();
		pages.VideosPage.CheckProjectName("Big-Bang-Theory");
	}	

	@Then("^New User opens User Menu$")
	public void registered_User_opens_User_Menu() throws Throwable {
	    pages.IndexPage.OpenUserMenu();
	}

	@Then("^User opens Settings page$")
	public void registered_User_opens_Settings_page() throws Throwable {
		pages.IndexPage.OpenUserMenu();
	    pages.IndexPage.OpenSettingsPage();
	    pages.SettingsPage.CheckDeletePage();
	}

	@Then("^User deletes Clickberry account$")
	public void registered_User_deletes_Clickberry_account() throws Throwable {
	   pages.SettingsPage.DeleteUserAccount();
	}

	@Then("^Deleted User logs out from Portal$")
	public void deleted_User_logs_out_from_Portal() throws Throwable {
	   pages.IndexPage.CheckUserLogout();
	}

	@Then("^Deleted FB User logs out from Facebook$")
	public void deleted_FB_User_logs_out_from_Facebook() throws Throwable {
	    pages.FacebookPage.FBLogout();
	}
	
	@When("^New Google User clicks Google icon$")
	public void new_Google_User_clicks_Google_icon() throws Throwable {
	    pages.SignUpPage.GoogleButtonClick();
	}

	@When("^Google Login window opens for new user$")
	public void google_Login_window_opens_for_new_user() throws Throwable {
	    pages.GooglePage.OpenGoogleSignUp();
	}

	@When("^New Google User enters Email and Password$")
	public void new_Google_User_enters_Email_and_Password() throws Throwable {
	    pages.GooglePage.GoogleUserLogin("click4qa@gmail.com", "qa123456789");
	}

	@Then("^Deleted Google User logs out from Google$")
	public void deleted_Google_User_logs_out_from_Google() throws Throwable {
	    pages.GooglePage.GoogleLogout();
	}
	
	@When("^New Twitter User clicks Twitter icon$")
	public void new_Twitter_User_clicks_Twitter_icon() throws Throwable {
	    pages.SignUpPage.TwitterButtonClick();
	}

	@When("^Twitter Login window opens for new user$")
	public void twitter_Login_window_opens_for_new_user() throws Throwable {
	    pages.TwitterPage.TwitterLoginOpen();
	}

	@When("^New Twitter User enters Email and Password$")
	public void new_Twitter_User_enters_Email_and_Password() throws Throwable {
	    pages.TwitterPage.TwitterLogin("qamary@mail.ru", "qa123456", "@QAMaryClick");
	}

	@Then("^Deleted Twitter User logs out from Twitter$")
	public void deleted_Twitter_User_logs_out_from_Twitter() throws Throwable {
	    pages.TwitterPage.TwitterLogout();
	}
	
	@When("^New VK User clicks VK icon$")
	public void new_VK_User_clicks_VK_icon() throws Throwable {
	    pages.SignUpPage.VkontakteButtonClick();
	}

	@When("^VK Login window opens for new user$")
	public void vk_Login_window_opens_for_new_user() throws Throwable {
	    pages.VkontaktePage.VKLoginOpen();
	}

	@When("^New VK User enters Email and Password$")
	public void new_VK_User_enters_Email_and_Password() throws Throwable {
	    pages.VkontaktePage.VkontakteLogin("79859870951", "qa123456");
	}

	@Then("^Deleted VK User logs out from VK$")
	public void deleted_VK_User_logs_out_from_VK() throws Throwable {
	    pages.VkontaktePage.VkontakteLogout();
	}
	
	@When("^New Email User enters Email, Password & Password Confirmation$")
	public void new_Email_User_enters_Email_Password_Password_Confirmation() throws Throwable {
	    pages.SignUpPage.UserCredentials("newuser@email.ru", "qa123456", "qa123456");
	}
	
	@When("^New Email User clicks Submit button$")
	public void new_Email_User_clicks_Submit_button() throws Throwable {
	    pages.SignUpPage.SubmitSignUp();
	    
	}    
	    
	@When("^New Email User enters Email and Password$")
	public void new_Email_User_enters_Email_and_Password() throws Throwable {
	    pages.SignInPage.EmailUserLogin("newuser@email.ru", "qa123456");   
		
	}
	


}
