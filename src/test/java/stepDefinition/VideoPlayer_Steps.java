package stepDefinition;

import cucumber.api.java.en.*;

public class VideoPlayer_Steps {
	
	@When("^User opens User Video$")
	public void user_opens_User_Video() throws Throwable {
		pages.IndexPage.OpenVideo();
	}

	@Then("^Video Player page is opened$")
	public void video_Player_page_is_opened() throws Throwable {
	    pages.PlayerPage.PlayerPageCheck();
	}

	@Then("^User closes Video Player page$")
	public void user_closes_Video_Player_page() throws Throwable {
	    pages.PlayerPage.ClosePlayerPage();
	}
	
	@When("^User enters Email and Password$")
	public void user_enters_Email_and_Password() throws Throwable {
	    pages.SignInPage.EmailUserLogin("projectsuser@mail.ru", "qa123456");
	}

	@When("^User opens video from the list$")
	public void user_opens_his_video() throws Throwable {
	    pages.VideosPage.OpenUserVideo();
	}
	
	@When("^User clicks on User Profile icon$")
	public void user_clicks_on_User_Profile_icon() throws Throwable {
	    pages.IndexPage.OpenUserVideoList();
	}

	@When("^User Videos page is opened$")
	public void user_Videos_page_is_opened() throws Throwable {
	    pages.VideosPage.CheckUserVideospage();
	}

	@Then("^User returns to Index page$")
	public void user_returns_to_Index_page() throws Throwable {
	    pages.IndexPage.ReturnHome();
	}

}
