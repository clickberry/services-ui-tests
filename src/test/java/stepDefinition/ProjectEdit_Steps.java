package stepDefinition;

import cucumber.api.java.en.*;

public class ProjectEdit_Steps {
	
	@When("^User opens Edit Video page$")
	public void user_opens_Edit_Video_page() throws Throwable {
	    pages.VideosPage.EditVideoOpen();
	}

	@Then("^User changes project name to Long Value$")
	public void user_changes_project_name_to_Long_Value() throws Throwable {
	    pages.VideosPage.ChangeProjectName("Veryyyyyy looooooooooong naaaaaaaaaaaame for teeeeeeeeeestiiiiiiiiiiiiiing veeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooong prooooooooooooooooooooooject naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaame");
	}

	@Then("^User saves changes$")
	public void user_saves_changes() throws Throwable {
	    pages.VideosPage.SubmitChanges();
	}

	@Then("^Project name is correctly changed$")
	public void project_name_is_correctly_changed() throws Throwable {
	    
	}

	@Then("^User changes project name to Empty Value$")
	public void user_changes_project_name_to_Empty_Value() throws Throwable {
		pages.VideosPage.ChangeProjectName("");
	}

	@Then("^User changes project name to Different Alphabet$")
	public void user_changes_project_name_to_Different_Alphabet() throws Throwable {
		pages.VideosPage.ChangeProjectName("مشروع جديد नई परियोजना 新項目");
	}

	@Then("^User changes project name to Symbols and Numbers$")
	public void user_changes_project_name_to_Symbols_and_Numbers() throws Throwable {
		pages.VideosPage.ChangeProjectName("~!@#$%^&*()_+ =-0987654321` }{|:<>?");
	}

	@Then("^User changes project name to Default Value$")
	public void user_changes_project_name_to_Default_Value() throws Throwable {
		pages.VideosPage.ChangeProjectName("Default Project Name");
	}
	
	@Then("^Project name is Long Value$")
	public void project_name_is_Long_Value() throws Throwable {		
	    pages.VideosPage.CheckProjectName("Veryyyyyy looooooooooong naaaaaaaaaaaame for teeeeeeeeeestiiiiiiiiiiiiiing veeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooong prooooooooooooooooooooooject naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaame");
	}

	@Then("^Project name is Different Alphabet$")
	public void project_name_is_Different_Alphabet() throws Throwable {		
		pages.VideosPage.CheckProjectName("مشروع جديد नई परियोजना 新項目");
	    
	}

	@Then("^Project name is Symbols and Numbers$")
	public void project_name_is_Symbols_and_Numbers() throws Throwable {		
		pages.VideosPage.CheckProjectName("~!@#$%^&*()_+ =-0987654321` }{|:<>?");
	    
	}

	@Then("^Project name is Default Value$")
	public void project_name_is_Default_Value() throws Throwable {		
		pages.VideosPage.CheckProjectName("Default Project Name");
	    
	}
	
	@Then("^User changes project name to Empty value$")
	public void user_changes_project_name_to_Empty_value() throws Throwable {
	    pages.VideosPage.ChangeProjectName("");
	}

	@Then("^Submit button is disabled$")
	public void submit_button_is_disabled() throws Throwable {
	    pages.VideosPage.SubmitDisabled();
	}

	@Then("^User cancels changes$")
	public void user_cancels_changes() throws Throwable {
	    pages.VideosPage.CancelEdit();
	    pages.VideosPage.CheckMyVideosPage();
	}
	
	@Then("^User changes project description to Long Value$")
	public void user_changes_project_description_to_Long_Value() throws Throwable {
	    pages.VideosPage.ChangeProjectDescription("Veryyyyyy looooooooooong deeeeeeeeeeeeeeeeeeeessssssssssssscccccccccccccccrrrrrrrrrrrrrrrrription for teeeeeeeeeestiiiiiiiiiiiiiing veeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooong prooooooooooooooooooooooject descriiiiiiiiiiiiiiiiiiiiptioooooooooon");
	}

	@Then("^Project description is Long Value$")
	public void project_description_is_Long_Value() throws Throwable {
	    pages.VideosPage.CheckProjectDescription("Veryyyyyy looooooooooong deeeeeeeeeeeeeeeeeeeessssssssssssscccccccccccccccrrrrrrrrrrrrrrrrription for teeeeeeeeeestiiiiiiiiiiiiiing veeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooong prooooooooooooooooooooooject descriiiiiiiiiiiiiiiiiiiiptioooooooooon");
	}

	@Then("^User changes project description to Different Alphabet$")
	public void user_changes_project_description_to_Different_Alphabet() throws Throwable {
		pages.VideosPage.ChangeProjectDescription("مشروع جديد नई परियोजना 新項目");
	}

	@Then("^Project description is Different Alphabet$")
	public void project_description_is_Different_Alphabet() throws Throwable {
		pages.VideosPage.CheckProjectDescription("مشروع جديد नई परियोजना 新項目");
	}

	@Then("^User changes project description to Symbols and Numbers$")
	public void user_changes_project_description_to_Symbols_and_Numbers() throws Throwable {
		pages.VideosPage.ChangeProjectDescription("~!@#$%^&*()_+ =-0987654321` }{|:<>?");
	}

	@Then("^Project description is Symbols and Numbers$")
	public void project_description_is_Symbols_and_Numbers() throws Throwable {
		pages.VideosPage.CheckProjectDescription("~!@#$%^&*()_+ =-0987654321` }{|:<>?");
	}

	@Then("^User changes project description to Default Value$")
	public void user_changes_project_description_to_Default_Value() throws Throwable {
		pages.VideosPage.ChangeProjectDescription("Default Project Description");
	}

	@Then("^Project description is Default Value$")
	public void project_description_is_Default_Value() throws Throwable {
		pages.VideosPage.CheckProjectDescription("Default Project Description");
	}
	
	@Then("^User changes project description to Empty Value$")
	public void user_changes_project_description_to_Empty_Value() throws Throwable {
		pages.VideosPage.ChangeProjectDescription("");
	}

	@Then("^Project description is Empty Value$")
	public void project_description_is_Empty_Value() throws Throwable {
		pages.VideosPage.CheckProjectDescription("");
	}
	
	@Then("^User changes project privacy to Hidden$")
	public void user_changes_project_privacy_to_Hidden() throws Throwable {
	    pages.VideosPage.ProjectHiddenTrue();
	}

	@Then("^Hidden video is not displayed$")
	public void hidden_video_is_not_displayed() throws Throwable {
		pages.IndexPage.ScrollFooter();
	    pages.IndexPage.CheckHiddenPrivate();
	}

	@Then("^User changes project privacy to Public$")
	public void user_changes_project_privacy_to_Public() throws Throwable {
	    pages.VideosPage.ProjectHiddenFalse();
	}

	@Then("^Public video is displayed$")
	public void public_video_is_displayed() throws Throwable {
		pages.IndexPage.ScrollFooter();
	    pages.IndexPage.CheckPublic();
	}
	
	@Then("^User navigates to Index page$")
	public void user_navigates_to_Index_page() throws Throwable {
	    pages.IndexPage.ReturnIndex();
	    pages.IndexPage.checkIndexPagesIsLoaded();
	}



}
