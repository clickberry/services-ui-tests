package stepDefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class UserProfile_Steps {	
	

	@Then("^User opens Profile page$")
	public void user_opens_Profile_page() throws Throwable {
		pages.IndexPage.OpenUserMenu();
	    pages.IndexPage.OpenProfilePage();
	    pages.UserProfilePage.checkProfilePage();
	}

	@Then("^User changes Name to Long value$")
	public void user_changes_Name_to_Long_value() throws Throwable {
	    pages.UserProfilePage.changeUserName("New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa");
	    pages.UserProfilePage.submitButtonClick();
	}
	
	@Then("^User Name is Long value$")
	public void user_Name_is_Long_value() throws Throwable {
		pages.IndexPage.ReturnIndex();
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenProfilePage();
	    pages.UserProfilePage.newUserNameCorrect("New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa");
	}

	@Then("^User changes Name to Different alphabet$")
	public void user_changes_Name_to_Different_alphabet() throws Throwable {
		pages.UserProfilePage.changeUserName("مشروع جديدनयाँ परियोजना新项目úéáćžä пользователь");
	    pages.UserProfilePage.submitButtonClick();
	}
	
	@Then("^User Name is Different alphabet$")
	public void user_Name_is_Different_alphabet() throws Throwable {
		pages.IndexPage.ReturnIndex();
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenProfilePage();
		pages.UserProfilePage.newUserNameCorrect("مشروع جديدनयाँ परियोजना新项目úéáćžä пользователь");
	}

	@Then("^User changes Name to Different symbols$")
	public void user_changes_Name_to_Different_symbols() throws Throwable {
		pages.UserProfilePage.changeUserName("~!@№$%^&()_+");
	    pages.UserProfilePage.submitButtonClick();
	}
	
	@Then("^User Name is Different symbols$")
	public void user_Name_is_Different_symbols() throws Throwable {
		pages.IndexPage.ReturnIndex();
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenProfilePage();
		pages.UserProfilePage.newUserNameCorrect("~!@№$%^&()_+");
	}

	@Then("^User changes Name to Numbers$")
	public void user_changes_Name_to_Numbers() throws Throwable {
		pages.UserProfilePage.changeUserName("123456789");
	    pages.UserProfilePage.submitButtonClick();
	}
	
	@Then("^User Name is Numbers$")
	public void user_Name_is_Numbers() throws Throwable {
		pages.IndexPage.ReturnIndex();
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenProfilePage();
		pages.UserProfilePage.newUserNameCorrect("123456789");
	}

	@Then("^User changes Name to Default value$")
	public void user_changes_Name_to_Default_value() throws Throwable {
		pages.UserProfilePage.changeUserName("Test User DONOTDELETE");
	    pages.UserProfilePage.submitButtonClick();
	}	

	@Then("^User Name is Default value$")
	public void user_Name_is_Default_value() throws Throwable {
		pages.IndexPage.ReturnIndex();
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenProfilePage(); 
		pages.UserProfilePage.newUserNameCorrect("Test User DONOTDELETE");
	}


	@And("^User changes Name to incorrect Empty value$")
	public void userChangesNameToIncorrectEmptyValue() throws Throwable {
		pages.UserProfilePage.changeUserName("");
	}

	@And("^Submit button should be disabled$")
	public void submitButtonShouldBeDisabled() throws Throwable {
		pages.UserProfilePage.submitButtonDisabled();
	}

	@And("^User changes Email to incorrect empty value$")
	public void userChangesEmailToIncorrectEmptyValue() throws Throwable {
		pages.UserProfilePage.changeUserEmail(" ");
	}

	@And("^User changes Email to incorrect autouser.mail.ru$")
	public void userChangesEmailToIncorrect_autouser_mail_ru() throws Throwable {
		pages.UserProfilePage.changeUserEmail("autouser.mail.ru");
	}

    @And("^User changes Email to incorrect @mail.ru$")
    public void userChangesEmailToIncorrect_mail_ru() throws Throwable {
        pages.UserProfilePage.changeUserEmail("@mail.ru");
    }

    @And("^User changes Email to incorrect autouser@$")
    public void userChangesEmailToIncorrect_autouser() throws Throwable {
        pages.UserProfilePage.changeUserEmail("autouser@");
    }

    @And("^User changes Email to incorrect autouser@mail..ru$")
    public void userChangesEmailToIncorrect_autouser_mail2_ru() throws Throwable {
        pages.UserProfilePage.changeUserEmail("autouser@mail..ru");
    }

    @And("^User changes Email to incorrect auto..user@mail.ru$")
    public void userChangesEmailToIncorrect_autouser_mail3_ru() throws Throwable {
        pages.UserProfilePage.changeUserEmail("auto..user@mail.ru");
    }

    @And("^User changes Email to incorrect autouser@.mail.ru$")
    public void userChangesEmailToIncorrect_autouser_mail4_ru() throws Throwable {
        pages.UserProfilePage.changeUserEmail("autouser@.mail.ru");
    }

    @And("^User changes Email to incorrect autouser@mail$")
    public void userChangesEmailToIncorrect_autouser_mail() throws Throwable {
        pages.UserProfilePage.changeUserEmail("autouser@mail");
    }

    @And("^User changes Email to incorrect auto user@mail.ru$")
    public void userChangesEmailToIncorrect_auto_user_mail_ru() throws Throwable {
        pages.UserProfilePage.changeUserEmail("auto user@mail.ru");
    }

    @And(value = "^User changes Email to incorrect symbols in the local part$")
    public void userChangesEmailToIncorrectSymbols() throws Throwable {
        pages.UserProfilePage.changeUserEmail("au(to)user@mail.ru");
    }

    @And("^User changes Email to incorrect auto@user@mail.ru$")
    public void userChangesEmailToIncorrect_auto_user_mail2_ru() throws Throwable {
        pages.UserProfilePage.changeUserEmail("auto@user@mail.ru");
    }


    @And("^User changes Email to already existing one$")
    public void userChangesEmailToAlreadyExistingOne() throws Throwable {
        pages.UserProfilePage.changeUserEmail("autouser2@mail.ru");
    }

    @And("^User clicks Submit button$")
    public void userClicksSubmitButton() throws Throwable {
        pages.UserProfilePage.submitButtonClick();
    }

    @And("^Error message is displayed$")
    public void errorMessageIsDisplayed() throws Throwable {
        pages.UserProfilePage.updateProfileMessage();
    }

    @And("^User clicks Cancel button$")
    public void userClicksCancelButton() throws Throwable {
        pages.UserProfilePage.cancelUpdate();
        pages.IndexPage.checkIndexPagesIsLoaded();
    }
}
