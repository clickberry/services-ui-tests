package stepDefinition;

import cucumber.api.java.en.*;

public class SignIn_Steps {		

	@When("^User navigates to SignIn page$")
	public void user_navigate_to_SignIn_page() {
		pages.IndexPage.OpenSignIn(); 
		pages.SignInPage.CheckSignIn();
	}

	@When("^FB User clicks Facebook icon$")
	public void user_clicks_Facebook_icon() {
		pages.SignInPage.FbButtonClick();
	}

	@When("^Facebook Login window opens$")
	public void facebook_Login_window_opens() {
		pages.FacebookPage.OpenFacebookLogin();
	}

	@When("^FB User enters Email and Password$")
	public void user_enters_Email_and_Password() {
		pages.FacebookPage.FacebookLogin("qajack@mail.ru", "qa123456");
	}
	
	@Then("^User is redirected to Index Page$")
	public void user_is_redirected_to_Index_Page() {
	    pages.IndexPage.checkIndexPagesIsLoaded();
	}

	@Then("^User is logged in$")
	public void user_Name_is_displayed() {
		pages.IndexPage.CheckUserLoggedIn();	    
	}

	@Then("^User navigates to Videos page$")
	public void my_Snaps_tab_is_displayed() {
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.OpenVideosPage();	
		pages.VideosPage.CheckMyVideosPage();
	}
/*
	@Then("^User My Likes tab is displayed$")
	public void my_Likes_tab_is_displayed() {
		pages.IndexPage.OpenMyLikesPage();	    
	} 
*/	
	@When("^User opens User Menu$")
	public void user_opens_User_Menu() throws Throwable {
		pages.IndexPage.OpenUserMenu();
	}
	
	@When("^User clicks Sign Out button$")
	public void user_clicks_Sign_Out_button() {
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.UserLogout();
	}
	/*
	@Then("^User clicks Sign Out button$")
	public void email_user_clicks_Sign_Out_button() {
		pages.IndexPage.OpenUserMenu();
		pages.IndexPage.UserLogout();
	}
	*/

	@Then("^User logs out from Portal$")
	public void user_logs_out_from_Portal() {
		pages.IndexPage.CheckUserLogout();
	}

	@Then("^FB User logs out from Facebook$")
	public void user_logs_out_from_Facebook() {
		pages.FacebookPage.FBLogout();
	}
	
	@When("^Google User clicks Google icon$")
	public void user_clicks_Google_icon() {
		pages.SignInPage.GoogleButtonClick();
	}

	@When("^Google Login window opens$")
	public void google_Login_window_opens() {
		pages.GooglePage.OpenGoogleLogin();
	}

	@When("^Google User enters Email and Password$")
	public void user_enters_Email_and_Password1() {
		pages.GooglePage.GoogleUserLogin("cat4qa@gmail.com", "qa123456789");
	}
	
	@Then("^Google User logs out from Google$")
	public void user_logs_out_from_Google() {
		pages.GooglePage.GoogleLogout();
	}
	
	@When("^Twitter User clicks Twitter icon$")
	public void twitter_User_clicks_Twitter_icon() throws Throwable {
	    pages.SignInPage.TwitterButtonClick();
	}

	@When("^Twitter Login window opens$")
	public void twitter_Login_window_opens() throws Throwable {
	    pages.TwitterPage.TwitterLoginOpen();
	}

	@When("^Twitter User enters Email and Password$")
	public void twitter_User_enters_Email_and_Password() throws Throwable {
	    pages.TwitterPage.TwitterLogin("qapaul@mail.ru", "qa123456", "@PaulClick2");
	}

	@Then("^Twitter User logs out from Twitter$")
	public void twitter_User_logs_out_from_Twitter() throws Throwable {
	    pages.TwitterPage.TwitterLogout();
	}

	@When("^VK User clicks VK icon$")
	public void vk_User_clicks_VK_icon() throws Throwable {
	    pages.SignInPage.VkontakteButtonClick();
	}

	@When("^VK Login window opens$")
	public void vk_Login_window_opens() throws Throwable {
	    pages.VkontaktePage.VKLoginOpen();
	}

	@When("^VK User enters Email and Password$")
	public void vk_User_enters_Email_and_Password() throws Throwable {
	    pages.VkontaktePage.VkontakteLogin("79175986240", "qa123456");
	}

	@Then("^VK User logs out from Twitter$")
	public void vk_User_logs_out_from_Twitter() throws Throwable {
	    pages.VkontaktePage.VkontakteLogout();
	}	
	
	@When("^Email User enters Email and Password$")
	public void email_User_enters_Email_and_Password() throws Throwable {
		pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
	}
	
	@When("^Email User enters Email autouser@mail\\.ru and Password$")
	public void email_User_enters_Email_autouser_mail_ru_and_Password() throws Throwable {
		pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
	}

	@When("^Email User enters Email newusertest@mail\\.com and Password$")
	public void email_User_enters_Email_newusertest_mail_com_and_Password() throws Throwable {
		pages.SignInPage.EmailUserLogin("newusertest@mail.com", "qa123456");
	}

	@When("^Email User enters Email EmailUserTest@mail\\.com and Password$")
	public void email_User_enters_Email_EmailUserTest_mail_com_and_Password() throws Throwable {
		pages.SignInPage.EmailUserLogin("EmailUserTest@Mail.com", "qa123456");
	}

	@When("^Email User enters Incorrect Email$")
	public void email_User_enters_Incorrect_Email() throws Throwable {
		pages.SignInPage.EmailUserLogin("someuser@mail.ru", "qa123456");
	}	

	@When("^Email User enters Incorrect Password$")
	public void email_User_enters_Incorrect_Password() throws Throwable {
		pages.SignInPage.EmailUserLogin("autouser@mail.ru", "11111111");
	}
	
	@Then("^Informational message is displayed$")
	public void informational_message_is_displayed() throws Throwable {
	    pages.SignInPage.LoginFail();
	}
	
	@Then("^User cancels login$")
	public void user_cancels_login() throws Throwable {
	    pages.SignInPage.LoginCancel();
	}

}
