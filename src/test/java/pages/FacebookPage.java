package pages;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static pages.IndexPage.ProjectName;

import com.codeborne.selenide.ElementsCollection;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.codeborne.selenide.SelenideElement;

public class FacebookPage {
	
	private static SelenideElement FacebookLoginForm = $("div.login_form_container");	
	private static SelenideElement FacebookEmailField = $(By.id("email"));
	private static SelenideElement FacebookPasswordField = $(By.id("pass"));
	private static SelenideElement FacebookLoginButton = $(By.id("loginbutton"));
	
	private static SelenideElement FacebookUserMenu = $(By.id("pageLoginAnchor"));
	private static SelenideElement FacebookLogoutButton = $(By.xpath("//li[contains(@data-gt, 'menu_logout')]/a"));
	
	private static SelenideElement FacebookPostLoginForm = $(By.id("loginform"));
	private static SelenideElement FBCreatePostImg = $(By.xpath("//a[contains(@href, 'clickberry')]//img"));
	private static SelenideElement FBPostButton = $(By.xpath("//button[@name='__CONFIRM__']"));
	private static ElementsCollection FBCreatedPostName = $$(By.xpath("//a[contains(@href, 'clickberry')]/div/div[1]"));
	//private static ElementsCollection FBCreatePostLink = $$(By.xpath("//a[contains(@href, 'clickberry')]"));
	
/**
 * Facebook login window opens.
 */
	
	public static void OpenFacebookLogin() {
		
		sleep(5000);
		FacebookLoginForm.should(exist);		
	}
	
/**
 * Login to Facebook.
 */
		
	public static void FacebookLogin(String Email, String Password) {  
		
		FacebookEmailField.sendKeys(Email);
		FacebookPasswordField.sendKeys(Password);
		sleep(2000);
		FacebookLoginButton.click();
		    
		}	

/**
 * Logout from Facebook account.
 */

	public static void FBLogout() {  	    
	    
	     open("http://facebook.com");
	     sleep(1000);
	     FacebookUserMenu.click(); 
	     sleep(10000);	        
	     FacebookLogoutButton.waitUntil(exist, 5000).click();
	     sleep(2000);	            
	}	
	
/**
  * Facebook login for post creation.	
  */
		
	public static void FacebookPostLogin() {
			
		 switchTo().window("Facebook");
		 sleep(1000);
		 FacebookPostLoginForm.should(exist);		
	}
	
/**
 * Facebook Post window is opened	
 */
	
public static void FBPostOpen() {
		
	    switchTo().window("Post to Facebook");
	    FBCreatePostImg.waitUntil(visible, 10000);		
	}
	
/**
 * Create post to Facebook 
 */
	
public static void FBCreatePost() {		    
	
		FBPostButton.click();
		
	}
		
	
/**
 * Check post was created on FB
 * @param ProjectName 
 */
	
	public static void FBCreatedPostCheck() {
		
		open("https://facebook.com");
		sleep(5000);
		refresh();
		FBCreatedPostName.get(0).shouldHave(text(ProjectName));
						
	}
			
	
}
