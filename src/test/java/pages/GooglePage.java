package pages;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.back;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;
import static pages.IndexPage.ProjectName;
import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.ElementsCollection;

import org.openqa.selenium.By;


public class GooglePage {
	
	private static SelenideElement GoogleLoginForm = $(By.xpath("//div[@class='main-content            no-name ']"));
	private static SelenideElement GoogleSignUpForm = $(By.xpath("//form[@id='gaia_loginform']"));	
	
	private static SelenideElement GoogleEmailField = $(By.id("Email"));
	private static SelenideElement GoogleNextButton = $(By.id("next"));
	private static SelenideElement GooglePasswordField = $(By.id("Passwd"));
	private static SelenideElement GoogleSignInButton = $(By.id("signIn"));
	private static SelenideElement GoogleProfileButton = $(By.xpath("//a[contains(@href, 'SignOutOptions')]"));
	private static SelenideElement GoogleSignOutButton = $(By.xpath("//a[contains(@href, 'Logout')]"));
	
	private static SelenideElement GooglePostImg = $(By.xpath("//a[contains (@href, 'clickberry')]/img"));
	private static SelenideElement GooglePostButton = $(By.xpath("//div[@guidedhelpid='sharebutton']"));
	private static ElementsCollection GoogleCreatedPostName = $$(By.xpath("//div[@role='article']//div[@class='rCauNb']//a[contains(@href, 'clickberry')]"));
	
/**
 * Google login window opens.
 */
		
	public static void OpenGoogleLogin() {
			
		GoogleLoginForm.waitUntil(visible, 5000);
			
		}	
	
/**
  * Google Sign Up window opens.
  */
			
    public static void OpenGoogleSignUp() {
				
    	GoogleSignUpForm.waitUntil(visible, 5000);
				
		}	  
    
    
/**
 * Login to Google
 */
	
	public static void GoogleUserLogin(String Email, String Password) { 
		
		GoogleEmailField.sendKeys(Email);
		GoogleNextButton.click();
		GooglePasswordField.sendKeys(Password);
		GoogleSignInButton.click();                
            
        }
	
/**
 * Logout from Google account.
 */

	public static void GoogleLogout() { 	
	    
	    open("http://gmail.com/");	    
	    GoogleProfileButton.waitUntil(visible,5000).click();
	    sleep(2000);
	    GoogleSignOutButton.waitUntil(visible,5000).click();
	    sleep(5000);	        
	    clearBrowserCache();  	       
	    
	    }	
	
/**
  * Google Sign In for post window opens.
  */
	   			
	    public static void OpenGoogleSignInPost() {
	   				
	    switchTo().window("Google+");  
	    GoogleSignUpForm.waitUntil(visible, 5000);
	   				
	    }	
	    
/**
 * Google Post window is opened	    
 */
	    public static void OpenGooglePost() {
	    	GooglePostImg.waitUntil(visible, 5000);
	    }
	    
/**
 * Google post create	    
 */
	    
	    public static void CreateGooglePost() {
	    	GooglePostButton.click();
	    }
	
/**
  * Check post was created on Google
  * @param ProjectName 
  */
	 	    
	    public static void GoogleCreatedPostCheck() {
	    		
	        open("https://plus.google.com/");
	    	sleep(5000);
	    	refresh();
	    	GoogleCreatedPostName.get(0).shouldHave(text(ProjectName));
	    						
	    	}	    
	    
	
/**
 * Open Gmail box.	
 */
	
	public static void OpenEmail(String Email, String Password) {
        // Open mail
        sleep(60000);
        open("http://gmail.com/");
        $(By.id("Email")).sendKeys(Email); 
        $(By.id("next")).click();
        $(By.id("Passwd")).sendKeys(Password);
        $(By.id("signIn")).click();
        sleep(10000);
        
   }
	
/**
 * Check Abuse Gmail letter.	
 */
   
   public static void CheckAbuseLetter() {

        // Open letter
        if ($(By.xpath("//table/tbody/tr/td[6]/div[@class='xS']")).exists() == true){
        	$$(By.xpath("//table/tbody/tr/td[6]/div[@class='xS']")).get(0).click();
        } 
        else{sleep(60000);
             refresh();
             $$(By.xpath("//table/tbody/tr/td[6]/div[@class='xS']")).get(0).click();
        } 
        sleep(5000);  
        $(By.xpath("//table/tbody/tr/td/table/tbody/tr[3]/td/h3")).shouldHave(text("Abuse Reported!"));
        
        // Close & delete letter
        back();
        $(By.xpath("//div[@class='Cq aqL']//span[@role='checkbox']")).setSelected(true);
        $(By.xpath("//div[@class='ar9 T-I-J3 J-J5-Ji']")).waitUntil(visible, 5000).click();

        sleep(5000);      
	}
	
   
/**
  * Check Feedback Gmail letter.	
  */
      
  public static void CheckFeedbackLetter(String Email, String Name, String Comment) {

           // Open letter
           if ($(By.xpath("//table/tbody/tr/td[6]/div[@class='xS']")).exists() == true){
           	$$(By.xpath("//table/tbody/tr/td[6]/div[@class='xS']")).get(0).click();
           } 
           else{sleep(60000);
                refresh();
                $$(By.xpath("//table/tbody/tr/td[6]/div[@class='xS']")).get(0).click();
           } 
           sleep(5000); 
           
           //Check letter 
           $(By.xpath("//div[@class='gs']//h1")).shouldHave(text(Name));
           $(By.xpath("//div[@class='gs']//p")).shouldHave(text(Comment));
           $(By.xpath("//div[@class='gs']//h3/span[1]")).shouldHave(text(Email));           
           
           // Close & delete letter
           back();
           $(By.xpath("//div[@class='Cq aqL']//span[@role='checkbox']")).setSelected(true);
           $(By.xpath("//div[@class='ar9 T-I-J3 J-J5-Ji']")).waitUntil(visible, 5000).click();

           sleep(5000);      
   	}   
  
  

}
