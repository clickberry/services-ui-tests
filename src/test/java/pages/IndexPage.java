package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Configuration.baseUrl;


/**
 * User UI: operations on Portal Index Page. 
 *
 */

public class IndexPage {	
	
	private static SelenideElement ClickberryLogo = $(By.xpath("//a[@ui-sref='home']"));	
	private static SelenideElement HomeButton = $(By.xpath("//a[@aria-label='Home']"));
	
	private static SelenideElement UserMenu = $(By.xpath("//md-menu-content"));	
	private static SelenideElement AuthorizeUserButton = $(By.xpath("//button[@aria-label='Authorize']"));	
	private static SelenideElement UserAccountButton = $(By.xpath("//button[@aria-label='Account']"));
	private static SelenideElement SignInButton = $(By.xpath("//a[@ui-sref='signin']/span"));
	private static SelenideElement SignUpButton = $(By.xpath("//md-menu-content//a[@ui-sref='signup']"));
	private static SelenideElement UserVideosButton = $(By.xpath("//md-menu-content//a[@ui-sref='my-videos']"));
	private static SelenideElement UserProfileButton = $(By.xpath("//md-menu-content//a[@ui-sref='profile']"));
	private static SelenideElement UserSettingsButton = $(By.xpath("//md-menu-content//a[@ui-sref='account-settings']"));
	private static SelenideElement SignOutButton = $(By.xpath("//button[@ng-click='signoff()']"));
	
	private static SelenideElement VideoList = $(By.xpath("//div[@class='video-grid layout-padding ng-scope ng-isolate-scope']"));
	private static ElementsCollection UserVideo = $$(By.xpath("//a[contains(@href, '#/show/')]"));
	private static ElementsCollection UserProfileImg = $$(By.xpath("//a[contains(@href, 'user')]"));
	
	private static ElementsCollection VideoShareButton = $$(By.xpath("//button[contains(@class, 'soc-share')]"));
	private static SelenideElement FBShareButton = $(By.xpath("//a[contains(@href, 'facebook')]"));
	private static SelenideElement VKShareButton = $(By.xpath("//a[contains(@href, 'vk')]"));
	private static SelenideElement TwitterShareButton = $(By.xpath("//a[contains(@href, 'twitter')]"));
	private static SelenideElement GoogleShareButton = $(By.xpath("//a[contains(@href, 'google')]"));
	
	public static String ProjectName = null;
	public static String UserName = null;
	private static SelenideElement UserProject = $(By.xpath("//figure/a[contains(@href, '56a0d3593e7f1114008397bd')]")); //project for testing privacy
	
	private static SelenideElement ContactsLink = $(By.xpath("//a[contains(@href, 'contacts')]"));	
	private static SelenideElement Footer = $(By.xpath("//footer"));
	private static SelenideElement LastProject = $(By.xpath("//a[contains(@href, '565ef396031cba1600b07546')]"));
	
	
	
		
/**
 * Open Index Page.	
 */
	
	public static void OpenIndex() {
		
	    open(baseUrl);	
	    checkIndexPagesIsLoaded();
	    
     }	
	
	
/**
 * Return on Index	
 */
	
	public static void ReturnIndex() {
		
		sleep(2000);
		ClickberryLogo.click();
		
	}
	
/**
 * Return Home	
 */
	
	public static void ReturnHome() {
		
		HomeButton.click();
		
	}
	
/**
  * Wait until index page is loaded (tabs are displayed). 
  */
	  
	  public static void checkIndexPagesIsLoaded() {
		  
		  VideoList.waitUntil(visible, 20000);
		 
      }
	  
/**
  * Check elements on the Index page.	
  */

	  	public static void CheckIndex() {		
	  		ClickberryLogo.should(exist);	
	  		AuthorizeUserButton.should(exist);	
	  		VideoList.should(exist);
	  	}
	  	
	  
/**
 * Open Registration page	
 */
	  	
	  public static void OpenRegistrationPage(){
		    AuthorizeUserButton.click();
		    sleep(1000);
	        SignUpButton.click();
	        		   
	  }
	  
/**
  * Open Sign In Page 
  */ 
	   
	  public static void OpenSignIn() {	
		    AuthorizeUserButton.click();
		    sleep(1000);
		    SignInButton.click();
		              	    
	  }		

	  
/**
 * Open Videos page	  
 */
	  
	  public static void OpenVideosPage() {	
		  sleep(2000);		  
		  UserVideosButton.click();		  		  		 
	  }
	  
/**
 * Open Profile page	  
 */
	  
	  public static void OpenProfilePage() {
		  sleep(2000);		  
		  UserProfileButton.click();		 
		  
	  }
	  
/**
  * Check User Logged In	  
  */
	  	  
	  	  public static void CheckUserLoggedIn() {		  
	  		  sleep(2000);
	  		  UserAccountButton.should(exist);
	  		 // UserVideosButton.should(exist);
	  		 // UserProfileButton.should(exist);
	  		 }	  
	  

/**
 * Open User Menu	
 */
	
	public static void OpenUserMenu(){		
		sleep(2000);
		//if (UserMenu.exists() == false){
		UserAccountButton.click();	    	
       // } 
		       		
	}	
	
/**
  * Open Settings page
  */	
	
	public static void OpenSettingsPage(){
		sleep(2000);
		UserSettingsButton.click();
	}
	  
/** 
  * Logout from Portal
  */

	public static void UserLogout() {
		sleep(2000);		
		SignOutButton.click();     
		
	}	
	
/**
 * Check User logged out	
 */
	public static void CheckUserLogout() {		
		sleep(2000);
		AuthorizeUserButton.should(exist);		
	}	
	
/**
 * Open User Video player	
 */
	
	public static void OpenVideo() {
		sleep(2000);
		UserVideo.get(0).click();
		//switchToWindow("Video on Clickberry");
	}
	
/**
 * Open User Videos List	
 */
	
	public static void OpenUserVideoList() {

		ReturnHome();
		sleep(2000);
		UserName = UserProfileImg.get(0).attr("title");
		UserProfileImg.get(0).click();
	}
	
/**
 * Click Share button	
 */
	
	public static void ClickShareButton() {
		
		ProjectName = UserVideo.get(0).getText();
		VideoShareButton.get(0).click();
				
	}
	
/**
 * Click FB Share button	
 */
	
	public static void FBShare() {
		
		FBShareButton.click();
	}
	
/**
  * Click VK Share button	
  */
		
	public static void VKShare() {
			
		VKShareButton.followLink();
			
	}
	
/**
  * Click VK Share button	
  */
			
	public static void TwitterShare() {
				
		TwitterShareButton.followLink();
				
	}
	
/**
  * Click Google Share button	
  */
				
	public static void GoogleShare() {
					
		GoogleShareButton.followLink();
					
		}	
	
/**
 * Hidden and Private projects are not displayed	
 */
	
	public static void CheckHiddenPrivate() {
		
		sleep(2000);
		UserProject.shouldNot(exist);
		
	}
	
/**
  * Public project is not displayed	
  */
		
	public static void CheckPublic() {
		
	
		sleep(2000);
		UserProject.should(exist);
			
	}	
	
/**
 * Open Feedback page	
 */
	public static void OpenContacts() {
		ContactsLink.click();
	}
	
/**
 * Scroll to Footer	
 */
	
	public static void ScrollFooter() {
		
		while(LastProject.exists() == false) {
			Footer.scrollTo();
		}
		
		
		
	}

}
