package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

public class PlayerPage {
	
	private static SelenideElement VideoPlayer = $(By.xpath("//div[@class='video-container']"));
	private static SelenideElement PlayerCloseButton = $(By.xpath("//div[@class='video-close']/button"));
	
/**
 * Check Player page is opened	
 */
	
	public static void PlayerPageCheck() {
		sleep(2000);
		VideoPlayer.waitUntil(visible, 20000);
		PlayerCloseButton.should(exist);
	}
	
/**
 * Close Player page	
 */
	
	public static void ClosePlayerPage() {
		sleep(2000);
		PlayerCloseButton.click();
		
	}

}
