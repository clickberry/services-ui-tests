package pages;

import org.openqa.selenium.*;

import ru.yandex.qatools.allure.annotations.Step;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * User UI: operations on User Profile page.
 *
 *
 */

public class UserProfilePage {
	
	private static SelenideElement pageTitle = $("h1");
	private static SelenideElement userAvatar = $(By.xpath("//div[@class='profile-avatar ng-scope']"));
		
	private static SelenideElement userNameField = $(By.name("profileName"));	
	private static SelenideElement emailField = $(By.name("profileEmail"));		
	private static SelenideElement submitButton = $(By.xpath("//button[@type='submit']"));
    private static SelenideElement cancelButton = $(By.xpath("//md-card-actions/a"));

    private static SelenideElement errorMessage = $(By.xpath("//div[@class='md-dialog-container ng-scope']/md-dialog"));
    private static SelenideElement errorMessageTitle = $(By.xpath("//div[@class='md-dialog-container ng-scope']/md-dialog//h2"));
    private static SelenideElement errorMessageCloseButton = $(By.xpath("//button[@ng-click='dialog.hide()']"));
		

/**
 * Check User Profile displays correctly (all fields present).
 */

    @Step("Check User Profile displays correctly (all fields present).")    
    public static void checkProfilePage() {
	
    	pageTitle.shouldHave(text("Profile details"));
    	userAvatar.shouldBe(visible);
    	userNameField.should(exist);
    	emailField.should(exist);	 
	
    }

/**
 * Check Submit Button is disabled.
 */

    @Step("Check Submit Button is disabled.")
    public static void submitButtonDisabled() {
    	submitButton.shouldBe(disabled); 	
    }
    
/**
 * Click Submit button.    
 */

    @Step("Click Submit button.")
    public static void submitButtonClick() {
    	submitButton.click();	
    }

/**
 * Click Cancel button.    
 */
    
    @Step("Click Cancel button.")
    public static void cancelUpdate() {
    	sleep(2000);
        cancelButton.click();
    }

/**
 * Change value in User Name field.
 * @param UserName
 */

    @Step("Change value in User Name field.")
    public static void changeUserName(String UserName) {
    	userNameField.clear();
    	userNameField.sendKeys(UserName); 
    }
    
/**
 * Check value in User Name field was correctly changed.    
 * @param correct
 */
    
    @Step("Check value in User Name field was correctly changed.")
    public static void newUserNameCorrect(String correct) {
    	refresh();
    	userNameField.shouldHave(value(correct));  	 
    }
    
/**
 * Change value in User Email field.  
 * @param Email
 */

    @Step("Change value in User Email field.")
    public static void changeUserEmail(String Email) {
    	emailField.clear();
    	emailField.sendKeys(Email);
    }
/**
 * Check Error message is displayed.
 */
    
    @Step("Check Error message is displayed.")
    public static void updateProfileMessage() {
    	errorMessage.should(exist);
    	errorMessageTitle.shouldHave(text("Profile update error"));
    	errorMessageCloseButton.click();

    }	    

}

