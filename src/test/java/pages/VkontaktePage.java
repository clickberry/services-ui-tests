package pages;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;
import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.ElementsCollection;

import static pages.IndexPage.ProjectName;

import org.openqa.selenium.By;

import com.codeborne.selenide.SelenideElement;

/**
 * Operations on VK side: login, logout, etc.
 * 
 */

public class VkontaktePage {
	
	private static SelenideElement VKLoginForm = $(By.id("login_submit"));
	private static SelenideElement VKEmailField = $(By.name("email"));
	private static SelenideElement VKPasswordField = $(By.name("pass"));
	private static SelenideElement VKLoginButton = $(By.id("install_allow"));
	
	private static SelenideElement VKPostImg = $(By.xpath("//div[@class='share_labeled']//img"));
	private static SelenideElement VKPostButton = $(By.id("post_button"));
	private static SelenideElement VKUserPage = $(By.id("myprofile"));
	private static ElementsCollection VKCreatedPostName = $$(By.xpath("//div[@class='wall_post_text']"));
	
/**
 * Open VK Login window.
 */
	
public static void VKLoginOpen() {
	
	VKLoginForm.waitUntil(visible, 5000);
	
}

/**
 * Login to VK account.
 * @param Phone
 * @param Password
 */
	
public static void VkontakteLogin(String Phone, String Password) { 	    
	    
	VKEmailField.waitUntil(visible, 5000).sendKeys(Phone);
	VKPasswordField.sendKeys(Password);
	VKLoginButton.click();	
	VKLoginForm.waitUntil(disappear, 240000);
	          
	  }

/**
 * Logout from Vkontakte account.
 */

public static void VkontakteLogout() { // VK User Logout	         
    
        open("http://vk.com");
        sleep(2000);
        $(By.id("logout_link")).click();  	    
        clearBrowserCache();	       
}

/**
 * Open VK Login window for post creating.
 */
	
public static void VKPostLoginOpen() {
	
	switchTo().window("VK | Login");
	VKLoginForm.waitUntil(visible, 5000);
	
}

/**
 * Open VK Post window
 */

public static void VKPostOpen() {
	
	//sleep(10000);
	//switchTo().window("VK | Share");
	sleep(5000);
	VKPostImg.shouldBe(visible);
	
}

/**
 * Create VK Post
 */

public static void VKPostCreate() {
	
	VKPostButton.click();
	
}

/**
 * Check post was created on VK
 * @param ProjectName 
 */
	
	public static void VKCreatedPostCheck() {
		
		open("http://vk.com");
		VKUserPage.click();
		VKCreatedPostName.get(0).shouldHave(text(ProjectName));
						
	}


}
