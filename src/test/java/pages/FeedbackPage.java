package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;


public class FeedbackPage {
	
	private static SelenideElement FeedbackForm = $(By.xpath("//h1"));
	
	private static SelenideElement EmailField = $(By.xpath("//input[@name='feedbackEmail']"));
	private static SelenideElement NameField = $(By.xpath("//input[@name='feedbackName']"));
	private static SelenideElement CommentField = $(By.xpath("//textarea[@name='feedbackComment']"));
	
	private static SelenideElement SubmitButton = $(By.xpath("//button[@type='submit']"));

	private static SelenideElement CancelButton = $(By.xpath("//md-card-actions//a"));
	
	private static SelenideElement SuccessDialog = $(By.xpath("//md-dialog//h2"));
	private static SelenideElement SuccessDialogConfirm = $(By.xpath("//md-dialog//button"));
	
	
/**
 * Check Feedback  page	
 */
	
	public static void CheckFeedback() {
		FeedbackForm.shouldHave(text("Clickberry Feedback"));
	}
	
/**
 * Fill Email field	
 */
	
	public static void EnterEmail(String Email) {
		EmailField.sendKeys(Email);
	}
	
/**
 * Fill User Name field	
 */
	
	public static void EnterName(String Name) {
		NameField.sendKeys(Name);
	}
	
/**
 * Fill Comment field	
 */
	
	public static void EnterComment(String Comment) {

		CommentField.setValue(Comment);

	}
	
/**
 * Submit sending Feedback	
 */
	
	public static void SendFeedback() {
		SubmitButton.click();
	}
	
/**
 * Submit button is disabled	
 */
	public static void FeedbackDisabled() {
		SubmitButton.shouldBe(disabled); 	
	}
	
/**
 * Feedback success dialog is displayed	
 */
	
	public static void FeedbackSucceed() {
		SuccessDialog.shouldHave(text("Thank you for your feedback"));
		SuccessDialogConfirm.click();
	}

	
/**
 * Cancel feedback creation	
 */
	
	public static void FeedbackCancel() {
		CancelButton.click();
	}

}
