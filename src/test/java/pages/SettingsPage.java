package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.SelenideElement;

/**
 * 
 * Base class with Settings operations: user delete, cancel user delete.
 *
 */

public class SettingsPage {
	
	private static SelenideElement DeleteForm = $(By.xpath("//form[@name='deleteform']"));
	private static SelenideElement DeleteButton = $(By.xpath("//form[@name='deleteform']//button[@type='submit']"));
	//private static SelenideElement CancelButton = $(By.xpath("//form[@name='deleteform']//a[@ui-sref='home']"));
	
	private static SelenideElement ConfirmationDialog = $(By.xpath("//md-dialog"));
	private static SelenideElement ConfirmationSubmitButton = $(By.xpath("//button[@ng-click='dialog.hide()']"));
	//private static SelenideElement ConfirmationCancelButton = $(By.xpath("//button[@ng-click='dialog.abort()']"));

/**
 * Check Settings page
 */ 

    public static void CheckDeletePage(){
    	DeleteForm.exists();
    }

/**
 * Delete User Account
 */
    
    public static void DeleteUserAccount(){
    	DeleteButton.waitUntil(visible, 5000).click();
    	ConfirmationDialog.should(exist);
    	sleep(2000);
    	ConfirmationSubmitButton.click();
    	
    }
    
    
    
    
 }
    
    
    
    
