package pages;

import com.codeborne.selenide.SelenideElement;

import org.openqa.selenium.*;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.ElementsCollection;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Configuration.baseUrl;

import static pages.IndexPage.UserName;

public class VideosPage {
	
	private static SelenideElement PageTitle = $("h1");	
	private static SelenideElement UserAvatar = $(By.xpath("//div[@class='profile-avatar ng-scope']"));
	private static SelenideElement NoVideos = $(By.xpath("//md-card//div[@class='flex-80']/p"));
	private static ElementsCollection VideosList = $$(By.xpath("//figure/a[contains (@ui-sref, 'id: project.id')]"));
	
	private static SelenideElement EditButton = $(By.xpath("//a[contains(@href, 'edit')]"));
	private static SelenideElement DeleteButton = $(By.xpath("//button[@aria-label='Delete']"));
	
	private static SelenideElement EditVideoPage = $(By.xpath("//md-card//h1"));
	private static SelenideElement ProjectNameField = $(By.xpath("//input[@name='projectName']"));
	private static SelenideElement ProjectDescriptionField = $(By.xpath("//input[@name='projectDescription']"));
	private static SelenideElement SetPrivate = $(By.xpath("//md-switch[contains(@ng-model, 'isPrivate')]//span"));
	private static SelenideElement SetHidden = $(By.xpath("//md-switch[contains(@ng-model, 'isHidden')]//span"));
	private static SelenideElement EditSubmit = $(By.xpath("//button[@type='submit']"));
	private static SelenideElement SubmitDisabled = $(By.xpath("//button[@disabled='disabled']"));
	private static SelenideElement EditCancel = $(By.xpath("//md-card-actions/a[@ui-sref='my-videos']"));
	
	private static ElementsCollection ProjectTitle = $$(By.xpath("//h3/a[contains(@ui-sref, 'project.id')]"));
	private static ElementsCollection UserProjectDescription = $$(By.xpath("//div[@class='sub-info ng-scope']/span[1]"));	


/**
 * Check Videos Page
 */

public static void CheckMyVideosPage() {
	
	sleep(2000);
	PageTitle.should(exist);
	PageTitle.shouldHave(text("My Videos"));
}

/**
 * Check User has no videos
 */

public static void CheckVideosAbsent() {
	
	NoVideos.should(exist);
	NoVideos.shouldHave(text("You don't have any videos yet. Start by creating one."));
	
}

/**
 * Open user video on My Videos page
 */

public static void OpenUserVideo() {
	
	VideosList.get(0).click();
	
}

/**
 * Check another user Videos Page
 */

public static void CheckUserVideospage() {
	
	sleep(2000);
	PageTitle.should(exist);
	PageTitle.shouldHave(text(UserName));
	//UserAvatar.should(exist);
	
}

/**
 * Open Edit Video page
 */

public static void EditVideoOpen() {
	sleep(2000);
	EditButton.click();
	EditVideoPage.shouldHave(text("Edit Video"));
}

/**
 * Change Project Name
 */

public static void ChangeProjectName(String ProjectName) {
	
	sleep(2000);
	ProjectNameField.setValue(ProjectName);	
	
}

/**
 * Check new project name
 */

public static void CheckProjectName(String ProjectName) {	
	
	sleep(2000);
	ProjectTitle.get(0).shouldHave(text(ProjectName));
	
}

/**
 * Change Project Description
 */

public static void ChangeProjectDescription(String ProjectDescription) {
	
	sleep(2000);
	ProjectDescriptionField.setValue(ProjectDescription);
	
}

/**
 * Check New project description
 */

public static void CheckProjectDescription(String ProjectDescription) {
	
	sleep(2000);
	UserProjectDescription.get(0).shouldHave(text(ProjectDescription));
	
}

/**
 * Set Hidden = true
 */

public static void ProjectHiddenTrue() {
	
	sleep(2000);
	SetHidden.click();	
	sleep(1000);
	SetHidden.shouldHave(text("Is Hidden: true"));
	
}

/**
 * Set Hidden = false
 */

public static void ProjectHiddenFalse() {
	
	sleep(2000);
	SetHidden.click();	
	sleep(1000);
	SetHidden.shouldHave(text("Is Hidden: false"));
}


/**
 * Save project changes
 */

public static void SubmitChanges() {
	
	EditSubmit.click();
}


/**
 * Changes are incorrect - Submit button is disabled.
 */

public static void SubmitDisabled() {
	
	sleep(2000);
	SubmitDisabled.should(exist);
	
}

/**
 * Cancel editing without saving changes
 */

public static void CancelEdit() {
	
	EditCancel.click();
}

}