package pages;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static pages.IndexPage.ProjectName;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.codeborne.selenide.ElementsCollection;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.codeborne.selenide.SelenideElement;

/**
 * Operations on Twitter side: login, logout, etc.
 *
 */

public class TwitterPage {
	
	private static SelenideElement TwitterLoginForm = $(By.id("oauth_form"));
	private static SelenideElement TwitterEmailField = $(By.id("username_or_email"));
	private static SelenideElement TwitterPasswordField = $(By.id("password"));
	private static SelenideElement TwitterAllowButton = $(By.id("allow"));
	private static SelenideElement TwitterUserMenu = $(By.xpath("//ul[@class='nav right-actions']/li"));
	private static SelenideElement TwitterLogoutButton = $(By.id("signout-form"));
	
	private static SelenideElement TwitterChallenge = $(By.id("challenge_response"));
	private static SelenideElement TwitterChallengeSubmit = $(By.id("email_challenge_submit"));	
	
	private static SelenideElement TwitterPostForm = $(By.id("update-form"));
	private static SelenideElement TwitterPostLoginButton = $(By.xpath("//input[@type='submit']"));
	
	private static SelenideElement TwitterPostTextField = $(By.id("status"));
	private static SelenideElement TwitterPostCreateButton = $(By.xpath("//div[@id='bd']//form//input[@type='submit']"));
	
	private static ElementsCollection TwitterPostFiles = $$(By.xpath("//div[@class='content']/div[@class='stream-item-footer']/a"));
	private static ElementsCollection TwitterCreatedPostName = $$(By.xpath("//div[@class='content']//p"));
	
/**
 * Open Twitter Login window	
 */
	
	public static void TwitterLoginOpen() {
		
		TwitterLoginForm.waitUntil(visible, 5000);
	}
	
/**
 * Login to Twitter account	
 */
	
	public static void TwitterLogin(String Email, String Password, String Name) {   	

		TwitterEmailField.sendKeys(Email);
		TwitterPasswordField.sendKeys(Password);
        TwitterAllowButton.click(); 
        if (TwitterAllowButton.exists() == true){
        	TwitterAllowButton.click();
        }   
        if (TwitterChallenge.exists() == true){
        	TwitterChallenge.sendKeys(Name);
        	TwitterChallengeSubmit.click();
        }
 
    }

/**
 * Logout from Twitter account.
 */

	public static void TwitterLogout() {  // Twitter User Logout	        
	    
	     open("http://twitter.com");
	     TwitterUserMenu.click();  
	     TwitterUserMenu.click();  
	     
	     JavascriptExecutor jse;
		 jse = (JavascriptExecutor)getWebDriver();
		 jse.executeScript("document.getElementById('signout-form').click()");
	     
	     //TwitterLogoutButton.click();
	     refresh();	                
	     clearBrowserCache();
	}	
	
/**
 * Twitter Post window open.	
 */
	
	public static void TwitterPostOpen() {
		TwitterPostForm.should(exist);		
	}
	
/**
  * Enter Twitter credentials for post
  */
		
		public static void TwitterPostLogin(String Email, String Password) {   	

			TwitterEmailField.sendKeys(Email);
			TwitterPasswordField.sendKeys(Password);
			TwitterPostLoginButton.click();	
	 
	    }	
		
/**
 * Create Twitter Post		
 */

		
		public static void TwitterPostCreate() {
				
			sleep(2000);
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			Date date = new Date();
			String currentdate = dateFormat.format(date);			
			
			TwitterPostTextField.sendKeys(currentdate);
			TwitterPostCreateButton.click();	
		}
		
/**
 * Open last post		
 */
		
		public static void TwitterLastPostOpen() {
			open("https://twitter.com");
			sleep(5000);
			refresh();
			TwitterPostFiles.get(0).click();
			
		}
		
/**
  * Check post was created on Twitter
  * @param ProjectName 
*/
			
	   public static void TwitterCreatedPostCheck() {				
		   open("https://twitter.com");
		   sleep(5000);
		   refresh();		 
		   TwitterCreatedPostName.get(0).shouldHave(text(ProjectName));
								
	   }		
}
