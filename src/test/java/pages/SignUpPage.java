package pages;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

import com.codeborne.selenide.SelenideElement;

/**
 * 
 * Base class with user registrations.
 *
 */

public class SignUpPage {	
	
	private static SelenideElement FacebookButton = $(By.xpath("//button[@ng-click='goFacebook()']"));
	private static SelenideElement VkontakteButton = $(By.xpath("//button[@ng-click='goVk()']"));
	private static SelenideElement GoogleButton = $(By.xpath("//button[@ng-click='goGoogle()']"));
	private static SelenideElement TwitterButton = $(By.xpath("//button[@ng-click='goTwitter()']"));
	
	private static SelenideElement UserEmailField = $(By.xpath("//input[@name='signupEmail']"));
	private static SelenideElement UserPasswordField = $(By.xpath("//input[@name='signupPassword']"));
	private static SelenideElement ConfirmPasswordField = $(By.xpath("//input[@name='signupConfirmPassword']"));
	
	private static SelenideElement SubmitButton = $(By.xpath("//button[@type='submit']"));
	private static SelenideElement CancelButton = $(By.xpath("//form[@name='signupform']//a[@ui-sref='home']"));
	
/**
 * Check Registration page	
 */
	public static void CheckRegistrationPage(){
		UserEmailField.should(exist);
		UserPasswordField.should(exist);
		ConfirmPasswordField.should(exist);
		
	}
	
/**
  * Click Facebook button.
  */
		
	public static void FbButtonClick() {  
		    FacebookButton.click();  	    
	 }	

/**
  * Click Vkontakte button.
  */

	public static void VkontakteButtonClick() { 	    
		    VkontakteButton.click();	   	      
	 }

/**
  * Click Google button.
  */

	public static void GoogleButtonClick() { 	
		    GoogleButton.click();             
	 }

/**
 * Click Twitter button.
 */

	public static void TwitterButtonClick() {  
		   TwitterButton.click();   
	 }
	
	
/**
 * Enter user credentials	
 */
	
	public static void UserCredentials(String Email, String Password, String Confirm) {
		
		UserEmailField.sendKeys(Email);
		UserPasswordField.sendKeys(Password);
		ConfirmPasswordField.sendKeys(Confirm);
	}

/**
 * Submit User Registration	
 */
	
	public static void SubmitSignUp() {
		
		SubmitButton.click();
	}
	
/**
 * Cancel User Registration	
 */
	
	public static void CancelSignUp() {
		
		CancelButton.click();
		
	}

}
