Feature: Share User Video
Description: User should be able to share project to social networks: FB, Twitter, G+, VK.   

@Share_1 @Share
Scenario: 5.1 Share User Video to Facebook from Index Page by not authorized user
   Given User is on Index Page
   And User clicks Share button for video
   And User selects sharing to Facebook
   Then FB Login window is opened
   And User enters FB credentials
   And FB Post window is opened
   And User created post to FB
   And User checks created post on FB
   And User goes back to Index page  
   
@Share_2 @Share
Scenario: 5.2 Share User Video to Twitter from Index Page by not authorized user
   Given User is on Index Page
   And User clicks Share button for video
   And User selects sharing to Twitter
   Then Twitter Post window is opened
   And User enters Twitter credentials
   And User creates post to Twitter
   And User checks created post on Twitter
   And User goes back to Index page      
   
@Share_3 @Share
Scenario: 5.3 Share User Video to Google from Index Page by not authorized user
   Given User is on Index Page
   And User clicks Share button for video
   And User selects sharing to Google
   Then Google Login window is opened
   And User enters Google credentials
   And Google Post window is opened
   And User created post to Google
   And User checks created post on Google
   And User goes back to Index page        
      
@Share_4 @Share
Scenario: 5.4 Share User Video to VK from Index Page by not authorized user
   Given User is on Index Page
   And User clicks Share button for video
   And User selects sharing to VK
   Then VK Login window is opened
   And User enters VK credentials
   And VK Post window is opened
   And User created post to VK
   And User checks created post on VK
   And User goes back to Index page    
   

   