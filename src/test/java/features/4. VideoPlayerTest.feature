Feature: View User Video
Description: User should be able to view Project as authorized/not authorized user

@Player_1 @Player
Scenario: 4.1 Open User Video Player from Index Page by not authorized user
   Given User is on Index Page
   When User opens User Video
   Then Video Player page is opened
   And User closes Video Player page
   
   
@Player_2 @Player
Scenario: 4.2 Open User Video Player from Index Page by authorized user
   Given User is on Index Page
   When User navigates to SignIn page
   And Email User enters Email and Password
   And User is redirected to Index Page
   And User opens User Video
   Then Video Player page is opened
   And User closes Video Player page
   And User clicks Sign Out button
   And User logs out from Portal
   
@Player_3 @Player
Scenario: 4.3 Open User Video Player from My Videos page
   Given User is on Index Page
   When User navigates to SignIn page
   And User enters Email and Password
   And User is redirected to Index Page
   And User navigates to Videos page 
   And User opens video from the list
   Then Video Player page is opened
   And User closes Video Player page
   And User clicks Sign Out button
   And User logs out from Portal  
   
@Player_4 @Player
Scenario: 4.4 Open User Video Player from User Videos Page by not authorized user
   Given User is on Index Page
   When User clicks on User Profile icon
   And User Videos page is opened
   Then User opens video from the list
   And Video Player page is opened
   And User closes Video Player page  
   And User returns to Index page  
   
@Player_5 @Player
Scenario: 4.5 Open User Video Player from User Videos Page by authorized user
   Given User is on Index Page
   When User navigates to SignIn page
   And Email User enters Email and Password
   And User is redirected to Index Page
   And User clicks on User Profile icon
   And User Videos page is opened
   Then User opens video from the list
   And Video Player page is opened
   And User closes Video Player page  
   And User returns to Index page    
   And User clicks Sign Out button
   And User logs out from Portal   
      