Feature: Login to Portal
Description: User should be able to login to Portal with Clickberry Email account or via Social Network (Facebook, Google, VK, Twitter).

@SignIn_1 @SignIn @regression
Scenario: 2.1 Login via Facebook
   Given User is on Index Page
   When User navigates to SignIn page
   And FB User clicks Facebook icon
   And Facebook Login window opens
   And FB User enters Email and Password
   Then User is redirected to Index Page
   And User is logged in
   And User navigates to Videos page   

@SignIn_2 @SignIn @regression   
Scenario: 2.2 Facebook User Logout
   Given User is on Index Page
   When User clicks Sign Out button
   Then User logs out from Portal
   And FB User logs out from Facebook     

@SignIn_3 @SignIn @regression 
Scenario: 2.3 Login via Google
   Given User is on Index Page
   When User navigates to SignIn page
   And Google User clicks Google icon
   And Google User enters Email and Password
   Then User is redirected to Index Page
   And User is logged in
   And User navigates to Videos page  

@SignIn_4 @SignIn @regression   
Scenario: 2.4 Google user Logout
   Given User is on Index Page
   When User clicks Sign Out button
   Then User logs out from Portal
   And Google User logs out from Google 
   
@SignIn_5 @SignIn @regression   
Scenario: 2.5 Login via Twitter
   Given User is on Index Page
   When User navigates to SignIn page
   And Twitter User clicks Twitter icon
   And Twitter Login window opens
   And Twitter User enters Email and Password
   Then User is redirected to Index Page
   And User is logged in
   And User navigates to Videos page  

@SignIn_6 @SignIn @regression   
Scenario: 2.6 Twitter user Logout
   Given User is on Index Page
   When User clicks Sign Out button
   Then User logs out from Portal
   And Twitter User logs out from Twitter 
   
@SignIn_7 @SignIn @regression   
Scenario: 2.7 Login via Vkontakte
   Given User is on Index Page
   When User navigates to SignIn page
   And VK User clicks VK icon
   And VK Login window opens
   And VK User enters Email and Password
   Then User is redirected to Index Page
   And User is logged in
   And User navigates to Videos page

@SignIn_8 @SignIn @regression   
Scenario: 2.8 VK user Logout
   Given User is on Index Page
   When User clicks Sign Out button
   Then User logs out from Portal
   And VK User logs out from Twitter
  
@SignIn_9 @SignIn @regression   
Scenario Outline: 2.9 Login via Email with correct credentials & logout
   Given User is on Index Page
   When User navigates to SignIn page
   And Email User enters Email <value> and Password
   Then User is redirected to Index Page
   And User is logged in
   And User navigates to Videos page 
   And User clicks Sign Out button
   And User logs out from Portal
   
   Examples:
    | value | description |
    | autouser@mail.ru | Email user |
    | newusertest@mail.com | Lower case |
    | EmailUserTest@mail.com | Upper case |
   
@SignIn_10 @SignIn
Scenario Outline: 2.10 Login via Email with incorrect credentials
   Given User is on Index Page
   When User navigates to SignIn page
   But Email User enters <value>
   Then Informational message is displayed
   And User cancels login
   
   Examples:
    | value | description |
    | Incorrect Email | someuser@mail.ru |
    | Incorrect Password | 11111111 |   
   
   
