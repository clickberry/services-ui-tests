Feature: Send User Feedback
Description: User should be able to send enail with feedback to Clickberry Support

@Feedback_1 @Feedback
Scenario Outline: 7.1 User sends Feedback email 
   Given User is on Index Page
   When User opens Feedback page
   And Feedback page is opened
   Then User enters <email> email
   And User enters <name> Name
   And User enters <comment> Comment
   And User sends Feedback
   And Clickberry Support receives <feedback> Feedback Email
   
   Examples:  
  | email | name | comment | feedback |
  | correct | correct | correct | correct |
  | long | long | long | long |
  | symbols | symbols | symbols | symbols |
       
@Feedback_2 @Feedback
Scenario Outline: 7.2 User fails to send Feedback email 
   Given User is on Index Page
   When User opens Feedback page
   And Feedback page is opened
   Then User enters <email> email
   And User enters <name> Name
   And User enters <comment> Comment
   And Feedback Submit button is disabled
   And User cancels sending feedback
   
   Examples:  
  | email | name | comment | 
  | incorrect | correct | correct |
  | absent | correct | correct |
  | correct| correct | incorrect | 
  | correct | correct | absent |    

