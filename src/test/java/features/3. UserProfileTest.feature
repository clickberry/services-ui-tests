Feature: Update data in User Profile
Description: User should be able to change personal data in the User Profile: User Name, Email.

@Profile_1 @Profile
Scenario Outline: 3.1 Change User Name to correct value
   Given User is on Index Page
   When User navigates to SignIn page
   And Email User enters Email and Password
   And User is redirected to Index Page
   Then User opens Profile page
   And User changes Name to <value>
   And User Name is <value>
   And User clicks Sign Out button
   And User logs out from Portal
   
   Examples: 
    | value | description |
    | Long value | New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa |
    | Different alphabet | مشروع جديدनयाँ परियोजना新项目úéáćžä пользователь |
    | Different symbols | ~!@№$%^&()_+ |
    | Numbers | 123456789 |
    | Default value | Test User DONOTDELETE |

@Profile_2 @Profile
Scenario Outline: Change User Name to incorrect value
   Given User is on Index Page
   When User navigates to SignIn page
   And Email User enters Email and Password
   And User is redirected to Index Page
   Then User opens Profile page
   And User changes Name to incorrect <value>
   And Submit button should be disabled
   And User clicks Sign Out button
   And User logs out from Portal
    
    Examples:
    | value | description |
    | Empty value |  |

@Profile_3 @Profile
Scenario Outline: Change User Email to incorrect value
  Given User is on Index Page
  When User navigates to SignIn page
  And Email User enters Email and Password
  And User is redirected to Index Page
  Then User opens Profile page
  And User changes Email to incorrect <value>
  And Submit button should be disabled
  And User clicks Sign Out button
  And User logs out from Portal
    
    Examples: 
    | value | description |
    |  empty value  |  |
    | autouser.mail.ru | No @ symbol in the email address |
    | @mail.ru | Address has no local part |
    | autouser@ | Address has no domain part |
    | autouser@mail..ru | Address contain consecutive dots (..) |
    | autouser@.mail.ru | Domain name begin with a dot |
    | auto user@mail.ru | Incorrect symbols in the local part |
    | auto@user@mail.ru | Incorrect symbols in the local part |
    | symbols in the local part | Incorrect symbols in the local part - au(to)user@mail.ru |

  @Profile_4 @Profile
  Scenario: Change User Email to already existing one
    Given User is on Index Page
    When User navigates to SignIn page
    And Email User enters Email and Password
    And User is redirected to Index Page
    Then User opens Profile page
    And User changes Email to already existing one
    And User clicks Submit button
    And Error message is displayed
    And User clicks Cancel button
    And User clicks Sign Out button
    And User logs out from Portal


  Scenario Outline: Change User Email to correct value
    Given User is on Index Page and logged in
    When User is on Profile Page
    And User enters new Email
    And new Email value is <correct>
    Then User clicks Change button
    And New User Email is <correct>
    And User logs out
    And User Logs in with new <correct> Email
    And User changes Email to default
    And User logs out
    
    Examples: 
    | correct | description |
    | 1autouser@mail.ru | Numbers in local part |
    | autouser@ma1il.ru | Numbers in the domain part |
    | auto.user@mail.ru | Dot in local part |
    | autouser@mail.new.ru | Dot in domain part |
    | auto-user@mail.ru | Hyphen in local part |
    | autouser@mail-new.ru | Hyphen in domain part |
    | auto_user@mail.ru | Underline in local part |
    



    

    
