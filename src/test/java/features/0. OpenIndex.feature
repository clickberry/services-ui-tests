Feature: Open Portal Index Page
Description: User should be able to open Portal Index Page

@Index @regression
Scenario: 1.0 Open Portal Index Page
   Given User is on Index Page
   Then Index Page is correctly displayed