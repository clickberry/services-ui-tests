Feature: Registration on Portal
Description: User should be able to register Clickberry account on Portal via Social Network (Facebook, Google, VK, Twitter) or create Clickberry Email account.

@SignUp_1 @SignUp
Scenario: 1.1 Register via Facebook
   Given User is on Index Page
   When New User navigates to Registration page
   And New FB User clicks Facebook icon
   And Facebook Login window opens for new user 
   And New FB User enters Email and Password
   Then New User is redirected to Index Page  
   And New User is logged in
   And New User Videos tab is displayed    
   
@SignUp_2 @SignUp
Scenario: 1.2 Delete Clickberry FB account
   Given User is on Index Page
   And New User is logged in
   Then User opens Settings page
   And User deletes Clickberry account   
   And Deleted User logs out from Portal
   And Deleted FB User logs out from Facebook  
   
@SignUp_3 @SignUp
Scenario: 1.3 Register via Google
   Given User is on Index Page
   When New User navigates to Registration page
   And New Google User clicks Google icon
   And Google Login window opens for new user 
   And New Google User enters Email and Password
   Then New User is redirected to Index Page  
   And New User is logged in 
   And New User Videos tab is displayed    
   
@SignUp_4 @SignUp
Scenario: 1.4 Delete Clickberry Google account
   Given User is on Index Page
   And New User is logged in
   Then User opens Settings page
   And User deletes Clickberry account
   And Deleted User logs out from Portal
   And Deleted Google User logs out from Google 
   
@SignUp_5 @SignUp
Scenario: 1.5 Register via Twitter
   Given User is on Index Page
   When New User navigates to Registration page
   And New Twitter User clicks Twitter icon
   And Twitter Login window opens for new user 
   And New Twitter User enters Email and Password
   Then New User is redirected to Index Page  
   And New User is logged in 
   And New User Videos tab is displayed    
   
@SignUp_6 @SignUp
Scenario: 1.6 Delete Clickberry Twitter account
   Given User is on Index Page
   And New User is logged in
   Then User opens Settings page
   And User deletes Clickberry account
   And Deleted User logs out from Portal
   And Deleted Twitter User logs out from Twitter    
   
@SignUp_7 @SignUp
Scenario: 1.7 Register via VK
   Given User is on Index Page
   When New User navigates to Registration page
   And New VK User clicks VK icon
   And VK Login window opens for new user 
   And New VK User enters Email and Password
   Then New User is redirected to Index Page  
   And New User is logged in
   And New User Videos tab is displayed    
   
@SignUp_8 @SignUp
Scenario: 1.8 Delete Clickberry VK account
   Given User is on Index Page
   And New User is logged in
   Then User opens Settings page
   And User deletes Clickberry account   
   And Deleted User logs out from Portal
   And Deleted VK User logs out from VK    
   
@SignUp_9 @SignUp
Scenario: 1.9 Register new Clickberry email account
   Given User is on Index Page
   When New User navigates to Registration page
   And New Email User enters Email, Password & Password Confirmation
   And New Email User clicks Submit button
   Then User is redirected to Index Page
   
   
@SignUp_10 @SignUp
Scenario: 1.10 Delete Clickberry Email account
   Given User is on Index Page
   When User navigates to SignIn page
   And New Email User enters Email and Password
   And User is redirected to Index Page
   Then User opens Settings page
   And User deletes Clickberry account   
   And Deleted User logs out from Portal     