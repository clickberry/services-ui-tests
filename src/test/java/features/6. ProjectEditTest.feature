Feature: Edit User Project
Description: User should be able to edit created projects: change privacy status, change project name & description, delete project.

@Edit_1 @Edit
Scenario Outline: 6.1 Edit project name to correct value
   Given User is on Index Page
   When User navigates to SignIn page
   And User enters Email and Password
   And User is redirected to Index Page
   And User navigates to Videos page 
   And User opens Edit Video page
   Then User changes project name to <correct>
   And User saves changes
   And Project name is <correct>   
   And User clicks Sign Out button
   And User logs out from Portal  
   
   Examples:
    | correct | description |
    | Long Value | Veryyyyyy looooooooooong naaaaaaaaaaaame for teeeeeeeeeestiiiiiiiiiiiiiing veeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooong prooooooooooooooooooooooject naaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaame |
    | Different Alphabet | مشروع جديد नई परियोजना 新項目 |
    | Symbols and Numbers | "~!@#$%^&*()_+ =-0987654321` }{|:<>?" |
    | Default Value | Default Project Name |
    
@Edit_2 @Edit
Scenario Outline: 6.2 Edit project name to incorrect value
   Given User is on Index Page
   When User navigates to SignIn page
   And User enters Email and Password
   And User is redirected to Index Page
   And User navigates to Videos page 
   And User opens Edit Video page
   Then User changes project name to <incorrect>
   And Submit button is disabled
   And User cancels changes   
   And User clicks Sign Out button
   And User logs out from Portal  
   
   Examples:
    | incorrect | description |  
    | Empty value | "" |
    
@Edit_3 @Edit
Scenario Outline: 6.3 Edit project description to correct value
   Given User is on Index Page
   When User navigates to SignIn page
   And User enters Email and Password
   And User is redirected to Index Page
   And User navigates to Videos page 
   And User opens Edit Video page
   Then User changes project description to <correct>
   And User saves changes
   And Project description is <correct>   
   And User clicks Sign Out button
   And User logs out from Portal  
   
   Examples:
    | correct | description |
    | Long Value | Veryyyyyy looooooooooong deeeeeeeeeeeeeeeeeeeessssssssssssscccccccccccccccrrrrrrrrrrrrrrrrription for teeeeeeeeeestiiiiiiiiiiiiiing veeeeeeeeeeeeeeeeeeeeeryyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooong prooooooooooooooooooooooject descriiiiiiiiiiiiiiiiiiiiptioooooooooon |
    | Different Alphabet | مشروع جديد नई परियोजना 新項目 |
    | Symbols and Numbers | "~!@#$%^&*()_+ =-0987654321` }{|:<>?" |
    | Empty Value | "" |
    | Default Value | Default Project Description |    
    
@Edit_4 @Edit
Scenario: 6.4 Edit project privacy to Hidden
   Given User is on Index Page
   When User navigates to SignIn page
   And User enters Email and Password
   And User is redirected to Index Page
   And User navigates to Videos page 
   And User opens Edit Video page
   Then User changes project privacy to Hidden
   And User saves changes
   And User navigates to Index page   
   And Hidden video is not displayed
   And User navigates to Videos page 
   And User opens Edit Video page
   And User changes project privacy to Public
   And User saves changes
   And User navigates to Index page   
   And Public video is displayed 
   And User clicks Sign Out button
   And User logs out from Portal     
        