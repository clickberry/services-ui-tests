package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import org.junit.Rule;

import selenium.util.PropertyLoader;

import com.codeborne.selenide.junit.ScreenShooter;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class UserPasswordChange_Steps {
	
	@Rule		
	public ScreenShooter screenShooter = ScreenShooter.failedTests();  
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }   
	
	
	@When("^User enters correct new password$")
	public void user_enters_correct_new_password() throws Throwable {
	    pages.UserProfilePage.EnterCurrentPassword("");
	}

	@When("^User enters correct confirmation$")
	public void user_enters_correct_confirmation() throws Throwable {
	    pages.UserProfilePage.EnterNewPassword("qa12345679");
	}

	@When("^User does not enter current password$")
	public void user_does_not_enter_current_password() throws Throwable {
	    pages.UserProfilePage.ConfirmNewPassword("qa12345679");
	}
	
	@When("^User enters incorrect current password$")
	public void user_enters_incorrect_current_password() throws Throwable {
		pages.UserProfilePage.EnterCurrentPassword("qa789456");
	}

}
