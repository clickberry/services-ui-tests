package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;
import static com.codeborne.selenide.Selenide.*;

import org.junit.Rule;

import selenium.util.PropertyLoader;

import com.codeborne.selenide.junit.ScreenShooter;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class SnapPage_Steps {	
	
	@Rule		
	public ScreenShooter screenShooter = ScreenShooter.failedTests();  
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }   
	
	/*
	@After 
	public static void LogOut() {
		pages.IndexPage.UserLogout();
	}
	*/
	
	@When("^User navigates to My Snaps Page$")
	public void user_navigates_to_My_Snaps_Page() throws Throwable {		
		pages.IndexPage.OpenVideosPage();
		pages.UserSnapsPage.CheckSnapsPage();
	}

	@When("^My Snaps Page is correctly displayed$")
	public void my_Snaps_Page_is_correctly_displayed() throws Throwable {
		pages.UserSnapsPage.CheckSnapsPage();
	    
	}

	@When("^User Snaps list is correctly displayed$")
	public void user_Snaps_list_is_correctly_displayed() throws Throwable {
		pages.UserSnapsPage.UserSnapsList();
	    
	}

	@When("^User opens Snap from My Snaps Page$")
	public void user_opens_Snap_from_My_Snaps_Page() throws Throwable {
	    pages.UserSnapsPage.OpenProject();
	}

	@Then("^My Snaps Page is displayed$")
	public void my_Snaps_Page_is_displayed() throws Throwable {
		pages.UserSnapsPage.CheckSnapsPage();
	}

	@Then("^User Snaps list is displayed$")
	public void user_Snaps_list_is_displayed() throws Throwable {
		pages.UserSnapsPage.UserSnapsList();
	}
	
	@When("^User navigates to other User Snaps Page$")
	public void user_navigates_to_other_User_Snaps_Page() throws Throwable {
	    pages.UserSnapsPage.OpenAnotherProjectsList();
	}

	@When("^User Snaps Page is correctly displayed$")
	public void user_Snaps_Page_is_correctly_displayed() throws Throwable {
	    pages.UserSnapsPage.CheckSnapsPage();
	}

	@When("^User opens Snap from other User Snaps Page$")
	public void user_opens_Snap_from_other_User_Snaps_Page() throws Throwable {
	    pages.UserSnapsPage.OpenProject();
	}

	@Then("^User Snaps Page is displayed$")
	public void user_Snaps_Page_is_displayed() throws Throwable {
		pages.UserSnapsPage.CheckSnapsPage();
	}
	
	@When("^User clicks Sign In button$")
	public void user_clicks_Sign_In_button() throws Throwable {
	    pages.SnapPage.SignInToComment();
	}
	
	@When("^Sign In page is opened$")
	public void sign_In_page_is_opened() throws Throwable {
	    pages.SignInPage.CheckSignIn();
	}
	
	@When("^User enters Email and Password$")
	public void user_enters_Email_and_Password() throws Throwable {
	    pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
	    sleep(5000);
	}

	@When("^User is redirected to Snap Page$")
	public void user_is_redirected_to_Snap_Page() throws Throwable {
	    pages.SnapPage.CheckSnapPage();
	}

	
	// ----------------------- comment --------------------------------------
	
	@When("^User goes to Comments field$")
	public void user_goes_to_Comments_field() throws Throwable {
	    pages.SnapPage.ClickComment();
	}

	@When("^User enters new Comment$")
	public void user_enters_new_Comment() throws Throwable {
	    pages.SnapPage.AddComment("This is a new test comment");
	}

	@Then("^User Comment is correctly displayed$")
	public void user_Comment_is_correctly_displayed() throws Throwable {
	    pages.SnapPage.CheckCreatedComment("This is a new test comment");
	}

	@Then("^User deletes created comment$")
	public void user_deletes_created_comment() throws Throwable {
	    pages.SnapPage.DeleteCreatedComment();
	}

	@Then("^Comment is correctly deleted$")
	public void comment_is_correctly_deleted() throws Throwable {
	    pages.SnapPage.CheckDeletedComment("This is a new test comment");
	}
	
	// ------------------------- Like -------------------------------------
	
	@When("^User clicks Like button$")
	public void user_clicks_Like_button() throws Throwable {
	    pages.SnapPage.LikeSnap();
	}

	@Then("^Like Icon is selected$")
	public void like_Icon_is_selected() throws Throwable {
	    pages.SnapPage.LikeCheck();
	}
/*
	@Then("^User goes to My Likes page$")
	public void user_goes_to_My_Likes_page() throws Throwable {
	    pages.IndexPage.OpenMyLikesPage();
	}
*/
	@Then("^Liked project is displayed on My Likes page$")
	public void liked_project_is_displayed_on_My_Likes_page() throws Throwable {
	    pages.MyLikesPage.CheckLike();
	}

	@Then("^User opens Snap from My Likes page$")
	public void user_opens_Snap_from_My_Likes_page() throws Throwable {
	    pages.MyLikesPage.OpenLikedProject();
	}

	@Then("^User cancels Like$")
	public void user_cancels_Like() throws Throwable {
	    pages.SnapPage.LikeCancel();
	}

	@Then("^Project is not displayed on My Likes page$")
	public void project_is_not_displayed_on_My_Likes_page() throws Throwable {
	    pages.MyLikesPage.CheckLikeCancel();
	}
	
	// -------------- Dislike --------------------------------------------
	
	@When("^User clicks Dislike button$")
	public void user_clicks_Dislike_button() throws Throwable {
	    pages.SnapPage.DislikeSnap();
	}

	@Then("^Dislike Icon is selected$")
	public void dislike_Icon_is_selected() throws Throwable {
	   pages.SnapPage.DislikeCheck();
	}

	@Then("^User cancels Dislike$")
	public void user_cancels_Dislike() throws Throwable {
	    pages.SnapPage.DislikeCancel();
	}
	
	// ------------------ Title ---------------------------------------
	
	@When("^User clicks Title field$")
	public void user_clicks_Title_field() throws Throwable {
	    pages.SnapPage.SelectProjectTitle();
	}

	@When("^User enters Project new title as new project title$")
	public void user_enters_Project_new_title_as_new_project_title() throws Throwable {
	    pages.SnapPage.AddNewTitle("Project new title");
	}

	@When("^User saves changes$")
	public void user_saves_changes() throws Throwable {
	    pages.SnapPage.SaveNewTitle();
	}

	@Then("^Project new title is displayed in project title field$")
	public void project_new_title_is_displayed_in_project_title_field() throws Throwable {
	    pages.SnapPage.CheckNewTitle("Project new title");
	}

	@When("^User enters Veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooooooooooooooooooooooooooong vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee as new project title$")
	public void user_enters_Veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy_loooooooooooooooooooooooooooooooooooooooooooooooong_vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee_as_new_project_title() throws Throwable {
		pages.SnapPage.AddNewTitle("Veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooooooooooooooooooooooooooong vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	}

	@Then("^Veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooooooooooooooooooooooooooong vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee is displayed in project title field$")
	public void veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy_loooooooooooooooooooooooooooooooooooooooooooooooong_vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee_is_displayed_in_project_title_field() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		pages.SnapPage.CheckNewTitle("Veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooooooooooooooooooooooooooong vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
	}
	
	// -------------------- Privacy -----------------------------------------
	
	@When("^User clicks Privacy field$")
	public void user_clicks_Privacy_field() throws Throwable {
	    pages.SnapPage.ChangePrivacy();
	}

	@Then("^Privacy is changed to Hidden$")
	public void privacy_is_changed_to_Hidden() throws Throwable {
	    pages.SnapPage.CheckPrivacyHidden();
	}

	@Then("^User clicks Privacy field again$")
	public void user_clicks_Privacy_field_again() throws Throwable {
		 pages.SnapPage.ChangePrivacy();
	}

	@Then("^Privacy is changed to Private$")
	public void privacy_is_changed_to_Private() throws Throwable {
	    pages.SnapPage.CheckPrivacyPrivate();
	}

	@Then("^Privacy is changed to Public$")
	public void privacy_is_changed_to_Public() throws Throwable {
	    pages.SnapPage.CheckPrivacyPublic();
	}
	
	// ---------------------------------------------------------- Abuse ------------------------------------------------------------------------------------------	
	
		// Почта, на которую приходит письмо про Abuse, настраивается в параметрах портала, 
		// если тест падает из-за того, что не доходит письмо, скорее всего проблема в настройках. 	
	
	@When("^User goes to Abuse tab$")
	public void user_goes_to_Abuse_tab() throws Throwable {
	   pages.SnapPage.ClickAbuseTab();
	}

	@When("^User sends Abuse Report$")
	public void user_sends_Abuse_Report() throws Throwable {
	    pages.SnapPage.SendReport();
	}

	@Then("^Informational message is displayed on the page$")
	public void informational_message_is_displayed_on_the_page() throws Throwable {
	    pages.SnapPage.CheckAbuseMessage();
	}

	@Then("^User goes to Abuse email box$")
	public void user_goes_to_Abuse_email_box() throws Throwable {
	    pages.GooglePage.OpenEmail();
	}

	@Then("^User checks Abuse letter$")
	public void user_checks_Abuse_letter() throws Throwable {
	    pages.GooglePage.CheckLetter();
	}
	
	@When("^User sends Abuse Report again$")
	public void user_sends_Abuse_Report_again() throws Throwable {
	   refresh();
	   pages.SnapPage.ClickAbuseTab();
	   pages.SnapPage.SendReport();	   
	}


}
