package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * User UI: operations on Trend Page.
 *
 */

public class TrendPage {
	
	private static SelenideElement TrendPageTitle = $(By.xpath("//div[@class='fix-content ng-scope']/h1"));
	private static SelenideElement TrendsSnapsList = $("div.list-tags");
	private static ElementsCollection TrendSnap = $$(By.xpath("//div[@ng-model='watch']/a"));
	
/**
 * Check Trends page is correctly displayed
 */
	
	public static void CheckTrendPage() {
		
		TrendPageTitle.should(exist); 
		TrendPageTitle.shouldHave(text("Trends"));
	}
	
/**
 * Check List of Snaps	
 */
	
	public static void CheckSnapsList() {
		TrendsSnapsList.shouldBe(visible); 
	}
	
/**
 * Open Snap from Trend Page.	
 */
	
	public static void OpenTrendProject() {
		
		TrendSnap.get(1).waitUntil(present, 5000).click();
	    
	}

}
