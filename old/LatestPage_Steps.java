package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import org.junit.Rule;

import selenium.util.PropertyLoader;

import com.codeborne.selenide.junit.ScreenShooter;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class LatestPage_Steps {
	
	@Rule		
	public ScreenShooter screenShooter = ScreenShooter.failedTests();  
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }   
/*	
	@When("^User navigates to Latest Page$")
	public void user_navigates_to_Latest_Page() throws Throwable {
	    pages.IndexPage.OpenLatestPage();
	}

	@When("^Latest Page is correctly displayed$")
	public void latest_Page_is_correctly_displayed() throws Throwable {
	    pages.LatestPage.CheckLatestPage();
	}

	@When("^Latest Snaps list is correctly displayed$")
	public void latest_Snaps_list_is_correctly_displayed() throws Throwable {
	    pages.LatestPage.LatestSnapsList();
	}

	@When("^User opens Snap from Latest Page$")
	public void user_opens_Snap_from_Latest_Page() throws Throwable {
	    pages.LatestPage.OpenLatestProject();
	}

	@Then("^Latest Page is displayed$")
	public void latest_Page_is_displayed() throws Throwable {
	    pages.LatestPage.CheckLatestPage();
	}

	@Then("^Latest Snaps list is displayed$")
	public void latest_Snaps_list_is_displayed() throws Throwable {
	    pages.LatestPage.LatestSnapsList();
	}
*/
}
