Feature: User Personal Snap
Description: User should be able to view & edit personal snaps.

@Personal @Personal_1 @regression
Scenario: Open Personal Snap from My Snaps page
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And My Snaps Page is correctly displayed
   And User Snaps list is correctly displayed
   And User opens Snap from My Snaps Page
   Then Snap is displayed
   And User closes Snap
   And My Snaps Page is displayed
   And User Snaps list is displayed
   And User logs out 

@Personal @Personal_2 @regression
Scenario: Add/delete comment to Personal Snap
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And User opens Snap from My Snaps Page
   And User goes to Comments field
   And User enters new Comment
   Then User Comment is correctly displayed
   And User deletes created comment
   And Comment is correctly deleted
   And User closes Snap
   And User logs out

@Personal @Personal_3 @regression
Scenario: Like & cancel Like to Personal Snap
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And User opens Snap from My Snaps Page
   And User clicks Like button
   Then Like Icon is selected
   And User closes Snap
   And User goes to My Likes page
   And Liked project is displayed on My Likes page
   And User opens Snap from My Likes page
   And User cancels Like
   And User closes Snap
   And Project is not displayed on My Likes page
   And User logs out  

@Personal @Personal_4 @regression
Scenario: Dislike & cancel Dislike to Personal Snap
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And User opens Snap from My Snaps Page
   And User clicks Dislike button
   Then Dislike Icon is selected
   And User cancels Dislike
   And User closes Snap
   And User logs out  

@Personal @Personal_5 @regression   
Scenario Outline: Edit Title for Personal Snap
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And User opens Snap from My Snaps Page
   And User clicks Title field
   And User enters <title> as new project title
   And User saves changes
   Then <title> is displayed in project title field
   And User closes Snap
   And User logs out   
   
 Examples:
    | title | description |
    | Project new title | New value |
    | Veeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeerrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy loooooooooooooooooooooooooooooooooooooooooooooooong vaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaluuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuueeeeeeeeeeeeeeeeeeeeeeeeeeeeee | Long value |

@Personal @Personal_6 @regression  
Scenario: Change privacy settings for Personal Snap
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And User opens Snap from My Snaps Page
   And User clicks Privacy field
   Then Privacy is changed to Hidden
   And User clicks Privacy field again
   And Privacy is changed to Private
   And User clicks Privacy field again
   And Privacy is changed to Public   
   And User closes Snap
   And User logs out   

@Personal @Personal_7 @regression  
Scenario: Send Abuse report for Personal Snap
   Given User is on Index Page
   And User is logged in
   When User navigates to My Snaps Page
   And User opens Snap from My Snaps Page
   And User goes to Abuse tab
   And User sends Abuse Report
   Then Informational message is displayed on the page
   And User closes Snap
   And User logs out 
   And User goes to Abuse email box
   And User checks Abuse letter
   And Google User logs out from Google 
    