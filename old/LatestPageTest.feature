Feature: Portal Latest Page
Description: User should be able to view snaps on Latest Page

@Latest @Latest_1 @regression
Scenario: Open Snap from Latest Page by unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to Latest Page
   And Latest Page is correctly displayed
   And Latest Snaps list is correctly displayed
   And User opens Snap from Latest Page
   Then Snap is displayed
   And User closes Snap
   And Latest Page is displayed
   And Latest Snaps list is displayed
 
@Latest @Latest_2 @regression
Scenario: Open Snap from Latest Page by authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to Latest Page
   And Latest Page is correctly displayed
   And Latest Snaps list is correctly displayed
   And User opens Snap from Latest Page
   Then Snap is displayed
   And User closes Snap
   And Latest Page is displayed
   And Latest Snaps list is displayed
   And User logs out
   
