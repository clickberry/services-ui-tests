Feature: Portal Trend Page
Description: User should be able to view snaps on Trend Page

@Trend @Trend_1 @regression
Scenario: Open Snap from Trends Page by unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to Trend Page
   And Trends Page is correctly displayed
   And Trending Snaps list is correctly displayed
   And User opens Snap from Trends Page
   Then Snap is displayed
   And User closes Snap
   And Trends Page is displayed
   And Trending Snaps list is displayed
 
@Trend @Trend_2 @regression
Scenario: Open Snap from Trends Page by authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to Trend Page
   And Trends Page is correctly displayed
   And Trending Snaps list is correctly displayed
   And User opens Snap from Trends Page
   Then Snap is displayed
   And User closes Snap
   And Trends Page is displayed
   And Trending Snaps list is displayed
   And User logs out
   
