package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

/**
 * User UI: operations on Latest Page.
 *
 */

public class LatestPage {
	
	private static SelenideElement LatestPageTitle = $(By.xpath("//div[@class='fix-content ng-scope']/h1"));
	private static SelenideElement LatestSnapsList = $("div.list-tags");
	private static ElementsCollection LatestSnap = $$(By.xpath("//div[@ng-model='watch']/a"));
	
	
/**
 * Open Latest Page.	
 */
	
	public static void CheckLatestPage() {		
		
		LatestPageTitle.should(exist);  
		LatestPageTitle.shouldHave(text("Latest"));  
	}
	
/**
 * Check List of Latest snaps	
 */
	
	public static void LatestSnapsList() {
		LatestSnapsList.should(exist);   
	}
	
/**
 * Open Snap from Latest Page.
 */
	
	public static void OpenLatestProject() {
		
		LatestSnap.get(3).waitUntil(present, 5000).click();
		
	}

}
