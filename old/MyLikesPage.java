package pages;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;

import org.openqa.selenium.By;

import com.codeborne.selenide.SelenideElement;

/**
 * Operations on My Likes page 
 * @author User
 *
 */

public class MyLikesPage {
	
	private static SelenideElement LikedProjectsList = $("div.list-tags");
	private static SelenideElement LikedProject = $(By.xpath("//div[@class='list-tags ng-scope']//a[contains(@href, '/user/likes/')]"));
	private static SelenideElement EmptyProjectList = $("div.list-tags-holder");
	
/**
 * Check Liked project is displayed	
 */
	public static void CheckLike() {
	    
    refresh();
    LikedProjectsList.waitUntil(visible, 10000);
    LikedProject.should(exist);    
    
	 }
	
/**
 * Open Project from Snaps Page.	
 */
		
	public static void OpenLikedProject() {
			
	LikedProject.click();		  
		
	}
			
	
/** 
 * Check project is not displayed after Like is canceled	
 */
	
	public static void CheckLikeCancel() {
	
    refresh();
    EmptyProjectList.waitUntil(visible, 10000);
    LikedProject.shouldNot(exist);		
	}

}
