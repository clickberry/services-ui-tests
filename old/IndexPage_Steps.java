package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.Rule;

import selenium.util.PropertyLoader;

import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.junit.ScreenShooter;
import com.google.common.io.Files;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class IndexPage_Steps {
	
	@Rule
	public ScreenShooter makeScreenshotOnFailure = ScreenShooter.failedTests().succeededTests(); 
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    } 	
	/*
	@Given("^Index Page is correctly displayed$")
	public void index_Page_is_correctly_displayed() throws Throwable {
	    pages.IndexPage.CheckIndex();
	}

	@When("^User navigates to Latest Tab$")
	public void user_navigates_to_Latest_Tab() throws Throwable {
	    pages.IndexPage.SelectLatestTab();
	}

	@When("^Snaps list is displayed$")
	public void snaps_list_is_displayed() throws Throwable {
	    pages.IndexPage.SnapsListDisplayed();
	}

	@When("^User opens Snap from Latest Tab$")
	public void user_opens_Snap_from_Latest_Tab() throws Throwable {
	    pages.IndexPage.OpenLatestSnap();
	}

	@Then("^Snap is displayed$")
	public void snap_is_displayed() throws Throwable {
	    pages.SnapPage.CheckSnapPage();
	}
	
	@Then("^User closes Snap$")
	public void user_closes_Snap() throws Throwable {
	    pages.SnapPage.CloseSnap();
	}

	@When("^User navigates to Trends Tab$")
	public void user_navigates_to_Trends_Tab() throws Throwable {
	    pages.IndexPage.SelectTrendTab();
	}

	@When("^User opens Snap from Trends Tab$")
	public void user_opens_Snap_from_Trends_Tab() throws Throwable {
	    pages.IndexPage.OpenTrendSnap();
	}

	@When("^User navigates to How it Works Tab$")
	public void user_navigates_to_How_it_Works_Tab() throws Throwable {
	    pages.IndexPage.SelectHowTab();
	}

	@Then("^User Guide is displayed$")
	public void user_Guide_is_displayed() throws Throwable {
	    pages.IndexPage.CheckUserGuide();
	}
*/
}
