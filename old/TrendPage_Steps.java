package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import org.junit.Rule;

import selenium.util.PropertyLoader;

import com.codeborne.selenide.junit.ScreenShooter;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class TrendPage_Steps {
	
	@Rule		
	public ScreenShooter screenShooter = ScreenShooter.failedTests();  
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }   
	
/*	
	@Given("^User is not logged in$")
	public void user_is_not_logged_in() throws Throwable {
	    pages.IndexPage.CheckUserLogout();
	}

	@When("^User navigates to Trend Page$")
	public void user_navigates_to_Trend_Page() throws Throwable {
	    pages.IndexPage.OpenTrendsPage();
	}

	@When("^Trends Page is correctly displayed$")
	public void trends_Page_is_correctly_displayed1() throws Throwable {
	    pages.TrendPage.CheckTrendPage();
	}

	@When("^User opens Snap from Trends Page$")
	public void user_opens_Snap_from_Trends_Page() throws Throwable {
	    pages.TrendPage.OpenTrendProject();
	}

	@Then("^Trends Page is displayed$")
	public void trends_Page_is_displayed() throws Throwable {
	    pages.TrendPage.CheckTrendPage();
	}

	@Given("^User is logged in$")
	public void user_is_logged_in() throws Throwable {
		pages.IndexPage.OpenIndex();
		pages.SignInPage.OpenSignIn();
		pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
	}
	
	@When("^Trending Snaps list is correctly displayed$")
	public void trending_Snaps_list_is_correctly_displayed() throws Throwable {
	    pages.TrendPage.CheckSnapsList();
	}

	@Then("^Trending Snaps list is displayed$")
	public void trending_Snaps_list_is_displayed() throws Throwable {
		 pages.TrendPage.CheckSnapsList();
	}
*/
}
