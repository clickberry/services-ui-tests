package pages;

import org.openqa.selenium.*;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import com.codeborne.selenide.ElementsCollection;


/**
 * User UI: operations on the Snap View page.
 * 
 *
 */

public class SnapPage {
	
	private static SelenideElement ProjectTitle = $("div.user-title");
	private static SelenideElement ProjectInfo = $("div.user-bottom");
	private static SelenideElement SnapPlayer = $("div.holder-media");
	private static SelenideElement CloseButton = $(By.xpath("//div[@class='popup popup-close ng-scope']"));
	
	private static SelenideElement AbuseTab = $(By.xpath("//li[@class='abuse right']/a"));
	private static SelenideElement AbuseTabSelected = $(By.xpath("//li[@class='abuse right active']"));
	private static SelenideElement ReportButton = $(By.xpath("//div[@class= 'btn btn-red btn-submit']/input"));
	private static SelenideElement AbuseTabText = $(By.xpath("//div[@class='tab active ng-scope']/h2"));
	private static SelenideElement AbuseSendMessage = $(By.xpath("//div[@class='tab active ng-scope']//p"));
	
	private static SelenideElement CommentTab = $(By.xpath("//li[@class='comments right']/a"));
	private static SelenideElement CommentTabSelected = $(By.xpath("//li[@class='comments right active']"));
	private static SelenideElement CommentForm = $(By.xpath("//div[@class='comments-text']"));
	private static SelenideElement AddCommentField = $("textarea.ng-pristine.ng-valid");
	private static SelenideElement PostButton = $(By.xpath("//div[@class='comments-text']//input[@type='submit']"));
	private static ElementsCollection CreatedComment = $$(By.xpath("//div[@class='comments-title ng-binding']"));
	private static ElementsCollection CreatedCommentText = $$(By.xpath("//div[@class='comments-text']/p"));
	private static ElementsCollection CommentDeleteButton = $$(By.xpath("//div[@class='comments-title ng-binding']/span[@class='delete-comment-button ng-scope']/a"));
	private static SelenideElement SignInButton = $("a.btn-green.ng-scope");
	
	private static SelenideElement LikeButton = $(By.xpath("//li[@class='like']/a"));
	private static SelenideElement LikeSelectedButton = $(By.xpath("//li[@class='like active']/a"));
	
	private static SelenideElement DislikeButton = $(By.xpath("//li[@class='dislike']/a"));
	private static SelenideElement DislikeSelectedButton = $(By.xpath("//li[@class='dislike active']/a"));
	
	private static SelenideElement TitleField = $(By.xpath("//div[@class='video-info']//div[@class='user-title']"));
	private static SelenideElement TitleFieldInput = $(By.xpath("//div[@class='video-info']//div[@class='user-title']//input"));
	private static SelenideElement EditTitleButton = $(By.xpath("//a[@ng-click='editTitle()']"));
	private static SelenideElement SaveTitleButton = $(By.xpath("//a[@ng-click='saveTitle(watch)']"));
	private static SelenideElement TitleFieldText = $(By.xpath("//div[@class='user-title']//span[@class='ng-binding']"));
	
	private static SelenideElement PrivacySwitch = $(By.xpath("//a[@ng-click='togglePublic(watch)']"));
	private static SelenideElement PublicIcon = $(By.xpath("//a[@ng-click='togglePublic(watch)']//i[@class='fa fa-globe']"));
	private static SelenideElement HiddenIcon = $(By.xpath("//a[@ng-click='togglePublic(watch)']//i[@class='fa fa-key']"));
	private static SelenideElement PrivateIcon = $(By.xpath("//a[@ng-click='togglePublic(watch)']//i[@class='fa fa-lock']"));
	
	
	
	
/** 
 * Check Data on Snap page
 */
	public static void CheckSnapPage() {
		ProjectInfo.waitUntil(visible, 40000);
		ProjectTitle.should(exist);
		SnapPlayer.should(exist);
	}
	
/**
 * Close Snap	
 */
    public static void CloseSnap() {
		CloseButton.click();
	}
    
// --------------------------------- Comment ------------------------------------------    
    
/**
 * Sign In to leave a comment.
 */
    	
    public static void SignInToComment() {		
    		
    	CommentTabSelected.should(exist);
    	SignInButton.click();		
    		
    }
    	    
    
/** 
 * Click Comments tab    
 */
    
public static void ClickComment() {
	
	 AbuseTab.click(); // workaround to check Comments button clicking
	 AbuseTabSelected.should(exist);

	 CommentTab.click();
	 CommentTabSelected.should(exist);
	
}

/**
 * Create new Comment
 */
    
public static void AddComment(String Comment){ 		
		
		if (SignInButton.exists() == true){
			pages.SnapPage.SignInToComment();
			pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
        }  	  
		else{
			refresh();
			AddCommentField.click();
		    AddCommentField.sendKeys(Comment);
		    PostButton.waitUntil(visible, 2000).click();
		}
		
}

/**
 * Check created comment
 * @param Comment
 */

public static void CheckCreatedComment(String Comment) {
	    
	    CommentForm.waitUntil(visible, 5000);
	    CreatedCommentText.get(0).shouldHave(text(Comment));	
			
	}

/**
 * Delete comment
 */

public static void DeleteCreatedComment() {
	
	    CreatedComment.get(0).hover();
	    CommentDeleteButton.get(0).click();
	    confirm("Do you really want to delete comment?");
	    
	
}

/**
 * Check comment was deleted
 * @param Comment
 */

public static void CheckDeletedComment(String Comment) {
	
	    CreatedCommentText.get(0).shouldNotHave(text(Comment));
}


	
// ----------------------------------------------------------- Like ---------------------------------------------------------------------------	

/**
 * Like user project.
 */

	public static void LikeSnap() {		
       
		LikeButton.waitUntil(exist, 2000).click();
       
	}
	
/** 
 * Check Like is set on Snap page.	
 */
	
	public static void LikeCheck() {
		refresh();
		LikeSelectedButton.should(exist);  
	}
	
	
/**
 * Cancel Like for project
 **/
	
	public static void LikeCancel() {
	    
		LikeSelectedButton.waitUntil(exist, 2000).click();
		LikeButton.waitUntil(exist, 2000);           
        
	}	
	
// ----------------------------------------------------------- Dislike ---------------------------------------------------------------------------	
	
/**
 * Snap Dislike.
 */
	
	public static void DislikeSnap() {		 
        
		DislikeButton.waitUntil(exist, 2000).click();	
	}
	
/**
 * Check Snap Dislike.	
 */
	
	public static void DislikeCheck() {
		
		DislikeSelectedButton.should(exist);     
	}
	
/**
 * Cancel Dislike for project
 **/
	
	public static void DislikeCancel() {	    
        
		DislikeSelectedButton.click();
		refresh();
        DislikeButton.should(exist);  
        
	}		
		
// ---------------------------------------------------------- Edit Title ------------------------------------------------------------------------------------
	
/**
 * Click on project title.	
 */
	public static void SelectProjectTitle() {
		TitleField.hover();
		EditTitleButton.click();
	}
	
/**
 * Change project title to new value.	
 * @param Title
 */
	
	public static void AddNewTitle(String Title) {		
		
		TitleFieldInput.clear();
		TitleFieldInput.sendKeys(Title);	
		
	}
	
/**
 * Save new project title.	
 */
	
	public static void SaveNewTitle() {
		SaveTitleButton.click();
	}
	
/**
 * Check new project title is correct.	
 * @param Title
 */
	
	public static void CheckNewTitle(String Title) {		
		
		TitleFieldText.shouldHave(text(Title));		
		
	}
	
// ---------------------------------------------------------- Privacy ----------------------------------------------------------------------------------------

/**
 * Change project privacy
 */
	
	public static void ChangePrivacy() {
		PrivacySwitch.click();
		sleep(2000);
	}	
	
/**
 * Check 'Hidden' project status.	
 */
	
	public static void CheckPrivacyHidden() {		
		HiddenIcon.should(visible);				
	}
	
/**
 * Check 'Private' project status.	
 */	
	
	public static void CheckPrivacyPrivate() {		
		PrivateIcon.should(visible);			
	}
	
/**
 * Check 'Public' project status.	
 */	
	
	public static void CheckPrivacyPublic() {
		PublicIcon.should(visible);		
	}
	
// ---------------------------------------------------------- Abuse ------------------------------------------------------------------------------------------	
	
	// Почта, на которую приходит письмо про Abuse, настраивается в параметрах портала, 
	// если тест падает из-за того, что не доходит письмо, скорее всего проблема в настройках. 	
	
/**
 * Go to Abuse tab.
 */		
	public static void ClickAbuseTab() {
		AbuseTab.click();
		AbuseTabSelected.should(exist);
		AbuseTabText.shouldHave(text("Report an abuse"));
	}
/**
 * Send Abuse Report	
 */
	
	public static void SendReport() {
		ReportButton.click();                
	}
	
/**
 * Check report sended.	
 */
	
	public static void CheckAbuseMessage() {
		ReportButton.should(disappear);
		AbuseSendMessage.shouldHave(text("Your report has been sent to administrator. Thank you."));
	}
        
   
 // ---------------------------------------------------------- Banner ------------------------------------------------------------------------
	
    /**
     * Check Clickberry Addon banner on the Snap View page.
     */
    	
    	//public static void SnapBanner() {
    		
    	//	$("div.video-option-banner").waitUntil(visible, 5000);
    	//	$("iframe[src='//ad.clickberry.tv/www/delivery/afr.php?zoneid=11&cb=INSERT_RANDOM_NUMBER_HERE']").should(exist);
    	//	//$("img[src^='//az411958.vo.msecnd.net/images/banner-video.png']").should(visible);
    	//}
    	
    // ----------------------------------------------------- Share -----------------------------------------------------------------------------------------	
    	
    /**
     * Check Share buttons on the Snap View page.
     */
    	
    	public static void SnapShares() {
    		
    		$("li.facebook").should(exist);
    		$("li.twitter").should(exist);
    		$("li.google-plus").should(exist);
    		$("li.vk").should(exist);
    		$("li.pinterest").should(exist);
    		$("li.blogger").should(exist);
    		$("li.tumblr").should(exist);
    		$("li.livejournal").should(exist);
    		$("li.linkedin").should(exist);
    		$("li.reddit").should(exist);
    		$("li.email").should(exist);
    		$(By.xpath("//div[@class='share-link ng-binding']")).should(exist);		
    	}
	
}
