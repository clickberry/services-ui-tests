Feature: Portal Index Page
Description: User should be able to view snaps on Index page.

@Index @Index_1 @regression
Scenario: Open Snap from Latest tab
   Given User is on Index Page
   And Index Page is correctly displayed
   When User navigates to Latest Tab
   And Snaps list is displayed
   And User opens Snap from Latest Tab
   Then Snap is displayed
   And User closes Snap
   And Snaps list is displayed
 
@Index @Index_2 @regression
Scenario: Open Snap from Trends tab  
   Given User is on Index Page
   And Index Page is correctly displayed
   When User navigates to Latest Tab
   And User navigates to Trends Tab
   And Snaps list is displayed
   And User opens Snap from Trends Tab
   Then Snap is displayed
   And User closes Snap
   And Snaps list is displayed
   
@Index @Index_3 @regression
Scenario: Open How it Works tab     
   Given User is on Index Page
   And Index Page is correctly displayed
   When User navigates to How it Works Tab
   Then User Guide is displayed