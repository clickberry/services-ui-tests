package stepDefinition;

import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Configuration.browser;

import org.junit.Rule;

import selenium.util.PropertyLoader;

import com.codeborne.selenide.junit.ScreenShooter;

import cucumber.api.java.Before;
import cucumber.api.java.en.*;

public class UserProfileUpdate_Steps {
	
	@Rule		
	public ScreenShooter screenShooter = ScreenShooter.failedTests();  
	
	@Before    
	  
	public static void setUp() { 		  
		  baseUrl = PropertyLoader.loadProperty("site.url");		  
		  browser = PropertyLoader.loadProperty("browser.name");				  
    }   
	/*
	@Given("^User is on Index Page and logged in$")
	public void user_is_logged_in() {
		pages.IndexPage.OpenIndex();
		pages.SignInPage.OpenSignIn();
		pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
	}
	
	@When("^User is on Profile Page$")
	public void user_is_on_Profile_Page() {
	    pages.UserProfilePage.OpenProfile();
	    
	}
	
	@Then("^User personal data is displayed$")
	public void user_personal_data_is_displayed() throws Throwable {
	    pages.UserProfilePage.CheckProfileInfo();
	}

	@Then("^Notification is displayed$")
	public void notification_is_displayed() throws Throwable {
	    pages.UserProfilePage.CheckNotification();
	}

	@When("^User enters new User Name$")
	public void user_enters_new_User_Name() {
		pages.UserProfilePage.ClearUserNameField();
	    
	}

	@When("^new User Name value is $")
	public void new_User_Name_value_is() {
		pages.UserProfilePage.ChangeUserName("");
	}

	@When("^new User Name value is Tooooo loooooong test name aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa$")
	public void new_User_Name_value_is_Tooooo_loooooong_test_name_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa() {
		pages.UserProfilePage.ChangeUserName("Tooooo loooooong test name aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	}
	
	@When("^new User Name value is New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa$")
	public void new_User_Name_value_is_New_very_long_test_user_name_for_testing_aaaaaaaaaaaaaaaaaaaaaaa() {
		pages.UserProfilePage.ChangeUserName("New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa");
	   	}
	
	@Then("^New User Name is New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa$")
	public void new_User_Name_is_New_very_long_test_user_name_for_testing_aaaaaaaaaaaaaaaaaaaaaaa() throws Throwable {
		pages.UserProfilePage.NewUserNameCorrect("New very long test user name for testing aaaaaaaaaaaaaaaaaaaaaaa");
	}
	
	@When("^new User Name value is Новый пользователь$")
	public void new_User_Name_value_is_Новый_пользователь() {
		pages.UserProfilePage.ChangeUserName("Новый пользователь");
	}

	@Then("^New User Name is Новый пользователь$")
	public void new_User_Name_is_Новый_пользователь() {
		pages.UserProfilePage.NewUserNameCorrect("Новый пользователь");
	}

	@When("^new User Name value is مشروع جديدनयाँ परियोजना新项目úéáćžä$")
	public void new_User_Name_value_is_مشروع_جديدनयाँ_परियोजना新项目úéáćžä() {
		pages.UserProfilePage.ChangeUserName("مشروع جديدनयाँ परियोजना新项目úéáćžä$");
	}

	@Then("^New User Name is مشروع جديدनयाँ परियोजना新项目úéáćžä$")
	public void new_User_Name_is_مشروع_جديدनयाँ_परियोजना新项目úéáćžä() {
		pages.UserProfilePage.NewUserNameCorrect("مشروع جديدनयाँ परियोजना新项目úéáćžä$");
	}

	@When("^new User Name value is ~!@№\\$%\\^&\\(\\)_\\+$")
	public void new_User_Name_value_is_$__() throws Throwable {
		pages.UserProfilePage.ChangeUserName("~!@№$%^&()_+");
	}

	@Then("^New User Name is ~!@№\\$%\\^&\\(\\)_\\+$")
	public void new_User_Name_is_$__() {
		pages.UserProfilePage.NewUserNameCorrect("~!@№$%^&()_+");
	}	
	
	@When("^new User Name value is (\\d+)$")
	public void new_User_Name_value_is(int arg1) throws Throwable {
		pages.UserProfilePage.ChangeUserName("123456789");
	}

	@Then("^New User Name is (\\d+)$")
	public void new_User_Name_is(int arg1) throws Throwable {
		pages.UserProfilePage.NewUserNameCorrect("123456789");
	}

	@When("^new User Name value is Test User DONOTDELETE$")
	public void new_User_Name_value_is_Test_User_DONOTDELETE() {
		pages.UserProfilePage.ChangeUserName("Test User DONOTDELETE");	    
	}
	
	@Then("^New User Name is Test User DONOTDELETE$")
	public void new_User_Name_is_Test_User_DONOTDELETE() {
		pages.UserProfilePage.NewUserNameCorrect("Test User DONOTDELETE");
	}	
		
	@When("^User enters new Country$")
	public void user_enters_new_Country() {
	    pages.UserProfilePage.ClearUserCountryField();
	}

	@When("^new Country value is Too long country name aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa$")
	public void new_Country_value_is_Too_long_country_name_aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa() throws Throwable {
	    pages.UserProfilePage.ChangeUserCountry("Too long country name aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	}
	
	@When("^new Country value is $")
	public void new_Country_value_is() {
	    pages.UserProfilePage.ChangeUserCountry("");
	}

	@Then("^New User Country is $")
	public void new_User_Country_is() throws Throwable {
	    pages.UserProfilePage.NewCountryCorrect("");
	}

	@When("^new Country value is Россия$")
	public void new_Country_value_is_Россия() throws Throwable {
	    pages.UserProfilePage.ChangeUserCountry("Россия");
	}

	@Then("^New User Country is Россия$")
	public void new_User_Country_is_Россия() throws Throwable {
	    pages.UserProfilePage.NewCountryCorrect("Россия");
	}

	@When("^new Country value is New very long country name for testing aaaaaaaaaaaaaaaaaaaaaaaaa$")
	public void new_Country_value_is_New_very_long_country_name_for_testing_aaaaaaaaaaaaaaaaaaaaaaaaa() throws Throwable {
	    pages.UserProfilePage.ChangeUserCountry("New very long country name for testing aaaaaaaaaaaaaaaaaaaaaaaaa");
	}

	@Then("^New User Country is New very long country name for testing aaaaaaaaaaaaaaaaaaaaaaaaa$")
	public void new_User_Country_is_New_very_long_country_name_for_testing_aaaaaaaaaaaaaaaaaaaaaaaaa() throws Throwable {
	    pages.UserProfilePage.NewCountryCorrect("New very long country name for testing aaaaaaaaaaaaaaaaaaaaaaaaa");
	}

	@When("^new Country value is ~!@№\\$%\\^&\\(\\)_\\+$")
	public void new_Country_value_is_$__() throws Throwable {
	    pages.UserProfilePage.ChangeUserCountry("~!@№$%^&()_+");
	}

	@Then("^New User Country is ~!@№\\$%\\^&\\(\\)_\\+$")
	public void new_User_Country_is_$__() throws Throwable {
	    pages.UserProfilePage.NewCountryCorrect("~!@№$%^&()_+");
	}

	@When("^new Country value is 中国$")
	public void new_Country_value_is_中国() throws Throwable {
	    pages.UserProfilePage.ChangeUserCountry("中国");
	}

	@Then("^New User Country is 中国$")
	public void new_User_Country_is_中国() throws Throwable {
	    pages.UserProfilePage.NewCountryCorrect("中国");
	}

	@When("^new Country value is USA$")
	public void new_Country_value_is_USA() throws Throwable {
		pages.UserProfilePage.ChangeUserCountry("USA");
	}

	@Then("^New User Country is USA$")
	public void new_User_Country_is_USA() throws Throwable {
		pages.UserProfilePage.NewCountryCorrect("USA");
	}
	
	
	// --------------- change email 
	
	@When("^User enters new Email$")
	public void user_enters_new_Email() throws Throwable {
	    pages.UserProfilePage.ClearUserEmailField();
	}
	
	// -------------- incorrect email

	@When("^new Email value is $")
	public void new_Email_value_is() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("");
	}

	@When("^new Email value is autouser.mail.ru$")
	public void new_Email_value_is_autouser_mail_ru1() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("autouser.mail.ru");
	}

	@When("^new Email value is @mail.ru$")
	public void new_Email_value_is_mail_ru() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("@mail.ru");
	}

	@When("^new Email value is autouser@$")
	public void new_Email_value_is_autouser() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("autouser@");
	}

	@When("^new Email value is autouser@mail..ru$")
	public void new_Email_value_is_autouser_mail_ru2() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("autouser@mail..ru");
	}

	@When("^new Email value is auto..user@mail.ru$")
	public void new_Email_value_is_auto_user_mail_ru1() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("auto..user@mail.ru");
	}

	@When("^new Email value is autouser@.mail.ru$")
	public void new_Email_value_is_autouser_mail_ru() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("autouser@.mail.ru");
	}

	@When("^new Email value is autouser@mail$")
	public void new_Email_value_is_autouser_mail() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("autouser@mail");
	}	

	@When("^new Email value is auto user@mail.ru$")
	public void new_Email_value_is_auto_user_mail_ru3() throws Throwable {
	   pages.UserProfilePage.ChangeUserEmail("auto user@mail.ru");
	}

	@When("^new Email value is au\\(to\\)user@mail\\.ru$")
	public void new_Email_value_is_au_to_user_mail_ru4() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("au(to)user@mail.ru");
	}

	@When("^new Email value is auto@user@mail.ru$")
	public void new_Email_value_is_auto_user_mail_ru5() throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("auto@user@mail.ru");
	}
	
	// ------------------------- 1autouser@mail.ru

	@When("^new Email value is (\\d+)autouser@mail\\.ru$")
	public void new_Email_value_is_autouser_mail_ru(int arg1) throws Throwable {
	    pages.UserProfilePage.ChangeUserEmail("1autouser@mail.ru");
	}

	@Then("^New User Email is (\\d+)autouser@mail\\.ru$")
	public void new_User_Email_is_autouser_mail_ru1(int arg1) throws Throwable {
	    pages.UserProfilePage.NewEmailCorrect("1autouser@mail.ru");
	}

	@Then("^User Logs in with new (\\d+)autouser@mail\\.ru Email$")
	public void user_Logs_in_with_new_autouser_mail_ru_Email1(int arg1) {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("1autouser@mail.ru", "qa123456");
	}
	
	@Then("^User changes Email to default$")
	public void user_changes_Email_to_default() throws Throwable {
		pages.UserProfilePage.OpenProfile();
		pages.UserProfilePage.ClearUserEmailField();
		pages.UserProfilePage.ChangeUserEmail("autouser@mail.ru");		
		pages.UserProfilePage.SaveChanges();
		pages.UserProfilePage.NewEmailCorrect("autouser@mail.ru");
	}		
	
	// ------------------------ autouser@mail1.ru
	@When("^new Email value is autouser@ma(\\d+)il\\.ru$")
	public void new_Email_value_is_autouser_ma_il_ru(int arg1) throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("autouser@ma1il.ru");
	}

	@Then("^New User Email is autouser@ma(\\d+)il\\.ru$")
	public void new_User_Email_is_autouser_ma_il_ru(int arg1) throws Throwable {
		pages.UserProfilePage.NewEmailCorrect("autouser@ma1il.ru");
	}

	@Then("^User Logs in with new autouser@ma(\\d+)il\\.ru Email$")
	public void user_Logs_in_with_new_autouser_ma_il_ru_Email(int arg1) throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("autouser@ma1il.ru", "qa123456");
	}
	
	// ---------------------- auto.user@mail.ru

	@When("^new Email value is auto\\.user@mail\\.ru$")
	public void new_Email_value_is_auto_user_mail_ru2() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("auto.user@mail.ru");
	}

	@Then("^New User Email is auto\\.user@mail\\.ru$")
	public void new_User_Email_is_auto_user_mail_ru2() throws Throwable {
		pages.UserProfilePage.NewEmailCorrect("auto.user@mail.ru");
	}

	@Then("^User Logs in with new auto\\.user@mail\\.ru Email$")
	public void user_Logs_in_with_new_auto_user_mail_ru_Email2() throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("auto.user@mail.ru", "qa123456");
	}
	
	// ---------------------- autouser@mail.new.ru

	@When("^new Email value is autouser@mail\\.new\\.ru$")
	public void new_Email_value_is_autouser_mail_new_ru3() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("autouser@mail.new.ru");
	}

	@Then("^New User Email is autouser@mail\\.new\\.ru$")
	public void new_User_Email_is_autouser_mail_new_ru3() throws Throwable {
		pages.UserProfilePage.NewEmailCorrect("autouser@mail.new.ru");
	}

	@Then("^User Logs in with new autouser@mail\\.new\\.ru Email$")
	public void user_Logs_in_with_new_autouser_mail_new_ru_Email3() throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("autouser@mail.new.ru", "qa123456");
	}

	// ----------------------- auto-user@mail.ru
	
	@When("^new Email value is auto-user@mail\\.ru$")
	public void new_Email_value_is_auto_user_mail_ru4() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("auto-user@mail.ru");
	}

	@Then("^New User Email is auto-user@mail\\.ru$")
	public void new_User_Email_is_auto_user_mail_ru4() throws Throwable {
		pages.UserProfilePage.NewEmailCorrect("auto-user@mail.ru");
	}

	@Then("^User Logs in with new auto-user@mail\\.ru Email$")
	public void user_Logs_in_with_new_auto_user_mail_ru_Email4() throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("auto-user@mail.ru", "qa123456");
	}
	
	// -------------------------- autouser@mail-new.ru

	@When("^new Email value is autouser@mail-new\\.ru$")
	public void new_Email_value_is_autouser_mail_new_ru() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("autouser@mail-new.ru");
	}

	@Then("^New User Email is autouser@mail-new\\.ru$")
	public void new_User_Email_is_autouser_mail_new_ru() throws Throwable {
		pages.UserProfilePage.NewEmailCorrect("autouser@mail-new.ru");
	}

	@Then("^User Logs in with new autouser@mail-new\\.ru Email$")
	public void user_Logs_in_with_new_autouser_mail_new_ru_Email() throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("autouser@mail-new.ru", "qa123456");
	}
	
	// ------------------------ auto_user@mail.ru

	@When("^new Email value is auto_user@mail\\.ru$")
	public void new_Email_value_is_auto_user_mail_ru() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("auto_user@mail.ru");
	}

	@Then("^New User Email is auto_user@mail\\.ru$")
	public void new_User_Email_is_auto_user_mail_ru() throws Throwable {
		pages.UserProfilePage.NewEmailCorrect("auto_user@mail.ru");
	}

	@Then("^User Logs in with new auto_user@mail\\.ru Email$")
	public void user_Logs_in_with_new_auto_user_mail_ru_Email() throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("auto_user@mail.ru", "qa123456");
	}
	
	// --------------------------- autouser@mail.ru

	@Then("^New User Email is autouser@mail\\.ru$")
	public void new_User_Email_is_autouser_mail_ru() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("autouser@mail.ru");
	}

	@Then("^User Logs in with new autouser@mail\\.ru Email$")
	public void user_Logs_in_with_new_autouser_mail_ru_Email() throws Throwable {
		pages.SignInPage.OpenSignIn();
	    pages.SignInPage.EmailUserLogin("autouser@mail.ru", "qa123456");
	}
 
	// ------------------------------------	
	
	@When("^such Email is already registered$")
	public void such_Email_is_already_registered() throws Throwable {
		pages.UserProfilePage.ChangeUserEmail("qajack@mail.ru");
	}

	@Then("^Error message is displayed$")
	public void error_message_is_displayed() throws Throwable {
	    pages.UserProfilePage.ChangeExistingEmail();
	}
		
	// ---------------------------------
	
	@Then("^Submit button should be disabled$")
	public void submit_button_should_be_disabled() {
		pages.UserProfilePage.ChangeButtonDisabled();
	    
	}	
	
	@Then("^User clicks Change button$")
	public void user_clicks_Change_button() {
		pages.UserProfilePage.SaveChanges();
	   
	}	
	
	@Then("^User logs out$")
	public void user_logs_out() {
	    pages.IndexPage.UserLogout();
	    pages.IndexPage.CheckUserLogout();
	}
	*/
}
