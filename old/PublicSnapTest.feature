Feature: Public User Snap
Description: User should be able to view other users snaps as not authorized user. 
User should be able to leave comments, likes/dislikes as authorized user.

@Public @Public_1 @regression
Scenario: Open Public Snap from User Snaps page as unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to other User Snaps Page
   And User Snaps Page is correctly displayed
   And User Snaps list is correctly displayed
   And User opens Snap from other User Snaps Page
   Then Snap is displayed
   And User closes Snap
   And User Snaps Page is displayed
   And User Snaps list is displayed
  
@Public @Public_2 @regression
Scenario: Open Public Snap from User Snaps page as authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to other User Snaps Page
   And User Snaps Page is correctly displayed
   And User Snaps list is correctly displayed
   And User opens Snap from other User Snaps Page
   Then Snap is displayed
   And User closes Snap
   And User Snaps Page is displayed
   And User Snaps list is displayed
   And User logs out 
   
@Public_3 @regression
Scenario: Add/delete comment to Public Snap as unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User goes to Comments field
   And User clicks Sign In button
   And Sign In page is opened
   And User enters Email and Password
   And User is redirected to Snap Page
   And User enters new Comment
   Then User Comment is correctly displayed
   And User deletes created comment
   And Comment is correctly deleted
   And User closes Snap
   And User logs out  
   
@Public @Public_4 @regression
Scenario: Add/delete comment to Public Snap as authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User goes to Comments field
   And User enters new Comment
   Then User Comment is correctly displayed
   And User deletes created comment
   And Comment is correctly deleted
   And User closes Snap
   And User logs out  
   
@Public @Public_5 @regression   
Scenario: Like & cancel Like to other user Snap as unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User clicks Like button
   And Sign In page is opened
   And User enters Email and Password
   And User is redirected to Snap Page
   And User clicks Like button
   Then Like Icon is selected
   And User closes Snap
   And User goes to My Likes page
   And Liked project is displayed on My Likes page
   And User opens Snap from My Likes page
   And User cancels Like
   And User closes Snap
   And Project is not displayed on My Likes page
   And User logs out         

@Public @Public_6 @regression   
Scenario: Like & cancel Like to other user Snap as authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User clicks Like button
   Then Like Icon is selected
   And User closes Snap
   And User goes to My Likes page
   And Liked project is displayed on My Likes page
   And User opens Snap from My Likes page
   And User cancels Like
   And User closes Snap
   And Project is not displayed on My Likes page
   And User logs out  

@Public @Public_7 @regression    
Scenario: Dislike & cancel Dislike to other user Snap as unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User clicks Dislike button
   And Sign In page is opened
   And User enters Email and Password
   And User is redirected to Snap Page
   And User clicks Dislike button
   Then Dislike Icon is selected
   And User cancels Dislike
   And User closes Snap
   And User logs out 

@Public @Public_8 @regression    
Scenario: Dislike & cancel Dislike to other user Snap as authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User clicks Dislike button
   Then Dislike Icon is selected
   And User cancels Dislike
   And User closes Snap
   And User logs out   
   
@Public @Public_9 @regression
Scenario: Send Abuse report for other user Snap as unauthorized user
   Given User is on Index Page
   And User is not logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User goes to Abuse tab
   And User sends Abuse Report
   And Sign In page is opened
   And User enters Email and Password
   And User is redirected to Snap Page
   And User sends Abuse Report again
   Then Informational message is displayed on the page
   And User closes Snap
   And User logs out 
   And User goes to Abuse email box
   And User checks Abuse letter
   And Google User logs out from Google 
 
@Public @Public_10 @regression     
Scenario: Send Abuse report for other user Snap as authorized user
   Given User is on Index Page
   And User is logged in
   When User navigates to other User Snaps Page
   And User opens Snap from other User Snaps Page
   And User goes to Abuse tab
   And User sends Abuse Report
   Then Informational message is displayed on the page
   And User closes Snap
   And User logs out 
   And User goes to Abuse email box
   And User checks Abuse letter
   And Google User logs out from Google    