package pages;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import org.openqa.selenium.By;

/**
 * User UI: operations on My Snaps Page.
 *
 */

public class UserSnapsPage {
	
	private static SelenideElement UserName = $("div.user-title");
	private static SelenideElement UserAvatar = $("span.img-title.img-title-user.ng-scope");
	private static SelenideElement SearchField = $("div.search-form");	
	private static SelenideElement UserSnapsList = $("div.list-tags.ng-scope");
	private static ElementsCollection UserSnap = $$(By.xpath("//div[@ng-model='watch']/a"));
	
/**
 * Open personal Snaps Page.
 */
	
	public static void CheckSnapsPage() {		 
		  
		  UserName.should(exist);
		  UserAvatar.should(exist);
		  SearchField.should(exist);
		  
	}
	
/**
 * Check User Snaps List	
 */
	
	public static void UserSnapsList() {
		UserSnapsList.should(exist);
	}
	
/**
 * Open Project from Snaps Page.	
 */
	
	public static void OpenProject() {
		
		UserSnap.get(0).waitUntil(present, 5000).click();		  
	}
	
/**
 * Open another user Snaps Page.	
 */
	
	public static void OpenAnotherProjectsList() {
		
		  $("a[href $= '/trends']").click();
		  $("div.list-tags").waitUntil(visible, 10000); 
		  $(By.xpath("//div[@class='fix-content ng-scope']//h1")).should(exist);
		  $$(By.xpath("//div[@class='user']/a[contains(@href, 'user')]")).get(1).click();
		  
	}

}
